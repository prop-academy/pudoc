// let name = 'Somebody'

// const newName = name;

// name = 'John'
// console.log(name)       // John
// console.log(newName)    // Somebody


/* With primitive types, a new assignegment creates another object in memory */

// const obj = {
//     name: 'Otherbody'
// }

// const newObj = obj;

// obj.name = 'John'
// console.log(obj.name)       // John
// console.log(newObj.name)    // John

/* With objects, the hold references to values in memory, so changing properties of objects normally will affect all other objects */

const obj = {
    30: 'years', 40: 'Years', name: 'Person'
}
console.log(obj[30])
console.log(obj['30'])
console.log(obj.name)

