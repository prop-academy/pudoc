function myMap(arr, callback) {
    let r = [];
    for(let i=0; i < arr.length; i++){
        r.push(callback(arr[i], i));
    }
    return r;
}

let myList = [1,2,3,5];

// myMap(myList, el => {return el * 2})
console.log(myMap(myList, el => {return el * 2}))