// ES5
function addNumber() {
    const args = arguments;
    // Or
    const args2 = Array.from(arguments) // make a list
    console.log('arguments ', args);
    console.log('arguments as list', args2);
}
console.log(addNumber(1, 2, 3));  // will log { '0': 1, '1': 2, '2': 3 }

// ES6
function addNumber2(...args) {
    console.log(args);
}
console.log(addNumber2(1, 2, 3, 4, 5, 6));   // will log [ 1, 2, 3, 4, 5, 6 ]

function addNumbers(a,b){
    return a+b;
}
console.log(addNumbers(1,2));

const addNumbers2 = (a,b) => a + b;
console.log(addNumbers2(1,2));

const upperCaseFun = (string) => string.toUpperCase(string)
console.log(upperCaseFun('something'));
