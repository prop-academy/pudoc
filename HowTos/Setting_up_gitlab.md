# How to use dedicated SSH Keys for a private gitlab instance

Create a new keypair to use for ssh authentification of the Gitlab Server, open your temrinal and type:

    cd ~/.ssh
    ssh-keygen -t ed25519 -C "email@example.com" -f my_git_server

The `-C` option is optional and can identify your key better by creating a comment inside the key. The `-f` option is the filename.
Display your public key and copy it with right click from the console:

    cat my_git_server.pub

**Add this ssh key to your Gitlab Account.**  

Now you have to configure your ssh to use this keypair for your host domain inside `./ssh/config` file.

    touch config
    nano config     # use gedit config or any other editor you have/like

Copy the following into `config`

    # Private GitLab instance
    Host gitlab.propulsion-home.ch
      Preferredauthentications publickey
      IdentityFile ~/.ssh/my_git_server

In my case I hade to set the file permission since I got an error `Bad owner or permissions on /home/user/.ssh/config`:

    chmod 600 ~/.ssh/config

If you used a non-default file path for your GitLab SSH key pair,
you must configure your SSH client to find your GitLab private SSH key
for connections to GitLab.

    ssh-add ~/.ssh/my_git_server


Test if it works:

    ssh -T git@gitlab.propulsion-home.ch
    # Welcome to GitLab, @new_user!

## References

- https://gitlab.propulsion-home.ch/help/ssh/README#working-with-non-default-ssh-key-pair-paths
- https://gitlab.propulsion-home.ch/help/ssh/README
