// Write a JavaScript function to check if a word is an anagram or not.

function isAnagram(test, original) {
    // just do a sort
    let sorted_test = test.toLowerCase().split('').sort().join('')
    let sorted_orig = original.toLowerCase().split('').sort().join('')

    // console.log (sorted_test, 'v.s.', sorted_orig)

    return sorted_test === sorted_orig

};

console.log(isAnagram("foefet", "toffee")) // true
console.log(isAnagram("Buckethead", "DeathCubeK")) // false, there is no D in Buckethead
console.log(isAnagram("Twoo", "WooT")) // true
console.log(isAnagram("dumble", "bumble"))// false
console.log(isAnagram("ound", "round"))// false
console.log(isAnagram("apple", "pale"))// false
