var canvas = document.querySelector('canvas');

var ctx = canvas.getContext('2d');

var width = canvas.width = window.innerWidth;
var height = canvas.height = window.innerHeight;

// Added an h4 to show which key id the pressed key has
// var keyInfo = document.getElementById('keycode-info');
var counter = 0;

// Create random number in the range of two numbers
function random(min, max) {
    var num = Math.floor(Math.random() * (max - min + 1)) + min;
    return num;
}

// Define function to update and display counter

countDisplay = document.getElementById('count');


function updateCounter(ball) {
    if (ball.exists) { counter += 1; }
    else { counter -= 1; };
    countDisplay.innerHTML = 'Ball count: ' + counter;
}

// The prototype Shape that Ball and evil ball inherits from

function Shape(x, y, velX, velY) {
    this.x = x;
    this.y = y;
    this.velX = velX;
    this.velY = velY;
    this.exists = true;
}

// The prototype ball object

function Ball(x, y, velX, velY, color, size) {

    Shape.call(this, x, y, velX, velY)

    this.color = color;
    this.size = size;
}

Ball.prototype = Object.create(Shape.prototype)
Object.defineProperty(Ball.prototype, 'constructor', {
    value: Ball,
    enumerable: true,
    writable: true
});

// method for Drawing the ball

Ball.prototype.draw = function () {
    // draw a shape
    ctx.beginPath();
    ctx.fillStyle = this.color;
    // make an arc shape, size~radius, start degree, end-degree
    ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
    ctx.fill();
}

// var testBall = new Ball(50, 100, 4, 4, 'blue', 10);
// testBall.draw();

Ball.prototype.update = function () {
    // Vector theory comes back here
    // Make a perpendicular vecor by just putting a "-" to either x or y.

    // Size is included so that ball is not swallowed at margins.
    if ((this.x + this.size) >= width) {
        this.velX = -(this.velX);
    }

    if ((this.x - this.size) <= 0) {
        this.velX = -(this.velX);
    }

    if ((this.y + this.size) >= height) {
        this.velY = -(this.velY);
    }

    if ((this.y - this.size) <= 0) {
        this.velY = -(this.velY);
    }
    // The velocity is the step increment to move the ball
    this.x += this.velX;
    this.y += this.velY;
}

// Collision detection

Ball.prototype.collisionDetect = function () {
    for (var j = 0; j < balls.length; j++) {
        if (!(this === balls[j])) {
            var dx = this.x - balls[j].x;
            var dy = this.y - balls[j].y;
            var distance = Math.sqrt(dx * dx + dy * dy);

            if (distance < this.size + balls[j].size) {
                balls[j].color = this.color = 'rgb(' + random(0, 255) + ',' + random(0, 255) + ',' + random(0, 255) + ')';
            }
        }
    }
}

// Animating the balls


/*
TUNING THE BALLS EXAMPLE WITH HUNGRY EVIL Circle, AND A SCORE HOW MANY BALLS ARE LEFT 
*/


function EvilCircle(x, y) {
    Shape.call(this, x, y, 30, 30, true);

    this.color = 'white';
    this.size = 50;
}

EvilCircle.prototype.draw = function () {
    ctx.lineWidth = 4;
    // draw a shape
    ctx.beginPath();
    ctx.fillStyle = this.color;
    // make an arc shape, size~radius, start degree, end-degree
    ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.strokeStyle = this.color
    // ctx.fill();
}

EvilCircle.prototype.checkBounds = function () {
    if ((this.x + this.size) >= width) {
        this.velX = -(this.velX);
    }

    if ((this.x - this.size) <= 0) {
        this.velX = -(this.velX);
    }

    if ((this.y + this.size) >= height) {
        this.velY = -(this.velY);
    }

    if ((this.y - this.size) <= 0) {
        this.velY = -(this.velY);
    }
}

function showKeyCode(code) {
    console.log(code);
}

EvilCircle.prototype.setControls = function () {
    /*
     we define a new variable for "this" with
    scope "setControls" to use inside window.onkeydown function
     */
    var _this = this;

    window.onkeydown = function (e) {
        // in here, "this" probabely points to the window object
        if (e.keyCode === 65) {
            _this.x -= _this.velX;
            // showKeyCode(65);
        } else if (e.keyCode === 68) {
            _this.x += _this.velX;
            // showKeyCode(68);
        } else if (e.keyCode === 87) {
            _this.y -= _this.velY;
            // showKeyCode(87);
        } else if (e.keyCode === 83) {
            _this.y += _this.velY;
            // showKeyCode(83);
        }
    };
};


EvilCircle.prototype.collisionDetect = function () {
    for (var j = 0; j < balls.length; j++) {

        if (balls[j].exists) {
            var dx = this.x - balls[j].x;
            var dy = this.y - balls[j].y;
            var distance = Math.sqrt(dx * dx + dy * dy);

            if (distance < this.size + balls[j].size) {
                balls[j].exists = false;
                // Ball counter, remove one
                // counter -= 1;
                // console.log('deleted a ball')
                updateCounter(balls[j]);
            }
        }

    }
}

// global scope
var balls = [];

// Instantiate EvilCircle

var blackHole = new EvilCircle(150, 150);
blackHole.setControls();

function loop() {
    // Canvas background color
    ctx.fillStyle = 'rgba(0, 0, 0, 0.25)';
    ctx.fillRect(0, 0, width, height);

    while (balls.length < 25) {
        var size = random(10, 20);
        var ball = new Ball(
            // ball position always drawn at least one ball width
            // away from the edge of the canvas, to avoid drawing errors
            // x
            random(0 + size, width - size),
            // y
            random(0 + size, height - size),
            // velX and velY
            random(-5, 2),
            random(-2, 5),
            // random color
            'rgb(' + random(0, 255) + ',' + random(0, 255) + ',' + random(0, 255) + ')',
            size
        );
        balls.push(ball);
        //   counter += 1;
        updateCounter(ball);
    }

    for (var i = 0; i < balls.length; i++) {
        if (balls[i].exists) {
            balls[i].draw();
            balls[i].update();
            //   Adding collision detection
            balls[i].collisionDetect();
        }

    }
    blackHole.draw();
    blackHole.checkBounds();
    blackHole.collisionDetect();
    // console.log('count: ', counter)
    // Loop function calls itself wrapped in the below function
    // when this method is constantly run and passed the same function name, 
    // it will run that function a set number of times per second to create a smooth animation. 
    requestAnimationFrame(loop);
}


var testBall = new Ball(50, 100, 4, 4, 'blue', 10);
console.log(testBall.constructor.prototype.constructor);
// testBall.draw();


// Release the balls...

loop()


