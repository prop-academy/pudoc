# Notes on Security

## Docker

- [Vulnerabilities in Docker images](https://snyk.io/blog/top-ten-most-popular-docker-images-each-contain-at-least-30-vulnerabilities/):
Take-away is that alpine-based images are better in terms of security because there are
    1. less system packages
    2. alpine is rebuilt when insecure linux os packages are known.
