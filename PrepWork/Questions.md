- When would you use a data structure like this?
    ```javascript
    let a = {
        key: '1',
        rest: {
            key: 2,
            rest: {
                key: 3,
                rest: ...
            }
        }
    }
    ```

- OOJS [in this file](OjectOrientedJS/ES2015_classes.js): 
    - Where are the methods greeting etc. stored in the Teacher object?
    - As soon an instance is initiated, function `farewell` can be seen under `snape.constructor.prototype`. Does Javascript go look inside it's constructor prototype to find a function if is not defined directly on the `Teacher.prototype` ? 

- Where are getters and setters handy? If default values are provided in the constructor for example? Otherwise every attribute can also be changed without settings getters and setters?

- [Example Project Bouncing Balls](https://github.com/mdn/learning-area/tree/master/javascript/oojs/bouncing-balls): If you would write it again now, would you use ES2015 classes to write it?

- Are there other differences for arrow functions except:
    - they don't bind their own `this`.

- Is there a difference between `Object.create(MyObject)` and `new MyObject`