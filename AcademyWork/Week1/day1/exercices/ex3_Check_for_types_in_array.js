// Create a function that checks whether all the element of an array are the same datatype


// check for typof does not help, 

function areSameType(arr) {

    function checkDataType(elem) {
        let type = typeof elem;
        if (type === 'object') {
            if (Array.isArray(elem)) return 'array';
        }
        return type;
    }

    let first_type = checkDataType(arr[0]);

    for (elem of arr) {
        if (checkDataType(elem) !== first_type) {
            console.log('false');
            return false;
        }
    }
    console.log('true');
    return true;
}



areSameType(['hello', 'world', 'long sentence']) // => true
areSameType([1, 2, 9, 10]) // => true
areSameType([['hello'], 'hello', ['bye']]) // => false
areSameType([['hello'], [1, 2, 3], [{ a: 2 }]]) // => true