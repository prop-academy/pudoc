
let a = 1;
const log = () => console.log(a)

function doSth() {
    a = 2;  // removed let
    const logger = log;
    return logger
}

a = 3;
const logger2 = doSth();
logger2()   // will print 2, but if a=3 is moved a line below.


/* The function remebers WHERE it was defined. It will try to access a from the top, but at the time of execution, a has changed to 5. */