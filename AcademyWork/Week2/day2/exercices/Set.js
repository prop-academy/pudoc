
// Constructor to initialize the set

// the best which is possible is O(1) for insert and remove.

/*

- All elements are unique, by default to objects are not unique and not sortable by default

Which is the best data structure?

If the value would be a hint to where to search the structure, insert and delete would be very efficient.

Binary Tree:

- Will add the items in a sorted way.
- Inserting and deleting is efficient.
- While inserting, checking for equality is easy.

 */

class BinaryItem {
    constructor(value) {
        this.value = value;
        this.smallerEl = null;
        this.biggerEl = null;
        this.parentEl = null;
    }

    hasChildren() {
        return this.biggerEl !== null && this.smallerEl !== null;
    }

    destroy() {
        delete this;
    }
}

class Set {

    constructor() {
        this.firstNode = null;
    }

    // inserts an element
    insert(element) {
        this.check(element);
        let item = new BinaryItem(element);
        if (this.firstNode === null) { this.firstNode = item; return; }

        let closestEl = this.findClosestInTree(element);

        if (item.value === closestEl.value) {
            throw `Elements is already in the set.`
        }
        if (item.value < closestEl.value) {
            closestEl.smallerEl = item;
        } else {
            closestEl.biggerEl = item;
        }
        item.parentEl = closestEl;
        return;
    }

    findClosestInTree(targetVal, startNode) {
        let elToCheck = startNode ? startNode : this.firstNode;

        while (true) {
            if (targetVal === elToCheck.value) return elToCheck

            if (targetVal < elToCheck.value) {
                if (elToCheck.smallerEl === null) break;
                elToCheck = elToCheck.smallerEl;
            } else {
                if (elToCheck.biggerEl === null) break;
                elToCheck = elToCheck.biggerEl;
            }
        }
        return elToCheck;
    }

    findSmallestInTree(startNode) {
        let elToCheck = startNode ? startNode : this.firstNode;
        while (elToCheck.smallerEl !== null) {
            elToCheck = elToCheck.smallerEl;
        }
        return elToCheck;
    }

    findSmallestNoChildren(startNode) {
        let elToCheck = startNode;
        while (elToCheck.hasChildren()) {
            if (elToCheck.biggerEl === null) elToCheck = elToCheck.smallerEl;
            elToCheck = this.findSmallestInTree(elToCheck)
        }
        return elToCheck;
    }

    findBiggestInTree(startNode) {
        // let elToCheck = startNode ? startNode : this.firstNode;
        while (startNode.biggerEl !== null) {
            startNode = startNode.biggerEl;
        }
        return startNode;
    }

    findBiggestNoChildren(startNode) {
        let elToCheck = startNode ? startNode : this.firstNode;
        while (elToCheck.hasChildren()) {
            if (elToCheck.smallerEl === null) elToCheck = elToCheck.biggerEl;
            elToCheck = this.findBiggestInTree(elToCheck)
        }
        return elToCheck;
    }

    // removes an element
    remove(element) {

        this.check(element);
        let toDelete = this.findClosestInTree(element);
        let parent = toDelete.parentEl;

        // Case: the element to delete is without children, just delete the link to it.
        if (!toDelete.hasChildren()) {
            if (toDelete.value > parent.value) {
                parent.biggerEl = null;
            } else {
                parent.smallerEl = null;
            }
            return;
        }

        // Case: element is not in the array (same check as has(element), but dont search twice)
        if (element !== toDelete.value) return null;

        // Case if the element has just one link, make direct attachment and return
        if (toDelete.smallerEl === null) {
            if (parent.value < toDelete.value) {
                parent.biggerEl = toDelete.biggerEl;
            } else {
                parent.smallerEl = toDelete.biggerEl;
            }
            toDelete.biggerEl.parentEl = parent;
            return;

        } else if (toDelete.biggerEl === null) {

            if (parent.value < toDelete.value) {
                parent.biggerEl = toDelete.smallerEl;
            } else {
                parent.smallerEl = toDelete.smallerEl;
            }
            toDelete.smallerEl.parentEl = parent;
            return;
        }

        // Case: toDelete has both links to further trees.
        let swapEl = this.findBiggestNoChildren(toDelete.smallerEl)

        // reassing the parent link, Swap El has no children
        if (swapEl.hasChildren()) throw 'This cant be, change code!'

        swapEl.biggerEl = toDelete.biggerEl;
        swapEl.smallerEl = toDelete.smallerEl;
        swapEl.parentEl = parent;

        // Reassing parents links based on his value with respect to toDelete
        if (toDelete.value === this.firstNode.value) {
            this.firstNode = swapEl;
        } else {
            if (toDelete.value > parent.value) {
                parent.biggerEl = swapEl;
            } else {
                parent.smallerEl = swapEl;
            }
            swapEl.parentEl = parent;
        }
    }

    // checks if an element exists in the set
    has(element) {
        this.check(element);
        let closestEl = this.findClosestInTree(element);
        return closestEl.value === element;
    }
    // returns array of all entries
    all() {
        if (!this.firstNode) return null;
        let r = [];
        function pushOutValue(item) {
            r.push(item.value)
            if (item.smallerEl !== null) { pushOutValue(item.smallerEl) }
            if (item.biggerEl !== null) { pushOutValue(item.biggerEl) }
        }
        pushOutValue(this.firstNode)
        return r;
    }

    // returns the number of all entries
    length() {
        if (!this.firstNode) return 0;
        let counter = 0;
        function countValue(item) {
            counter++;
            if (item.smallerEl !== null) { countValue(item.smallerEl) }
            if (item.biggerEl !== null) { countValue(item.biggerEl) }
            return counter
        }
        return countValue(this.firstNode)
        
    }

    check(element) {
        let to = typeof element;
        if (to !== "string" && to !== "number") throw 'Just primitive values can be hold in the Set.'
    }
}


let myset = new Set();

let myNumbers = [8, 6, 10, 5, 7, 9, 15, 1, 5.25, 8.5, 9.5, 20, 21, 22, 19, 17, 19.5, 6.9, 7.5, 2, 0];

for (n of myNumbers) {
    myset.insert(n)
};

console.log(myset.has(6)) // true
console.log(myset.has(7)) // true
console.log(myset.has(5)) // true
// myset.remove(7)
// console.log(myset.has(7)) // false
console.log(myset.has(19)) // true
console.log(myset.has(1)) // true
console.log(myset.has(6.5)) // false
console.log(myset.has(6.9)) // true
console.log(myset.all().length === myNumbers.length) // true
console.log(myset.length() === myNumbers.length) // true

// last check if all the link work and no errors
// myset.remove(8);
// console.log(myset.has(8)) // false
// myset.insert(8);
// console.log(myset.firstNode.value) // 7.5
// console.log(myset.has(6)) // true
// console.log(myset.has(10)) // true
// console.log(myset.has(10)) // true
for (n of myNumbers) myset.remove(n);
