'use strict';

function initialDirection(speed) {

    let x = Math.random(1);
    let y = Math.random(1);
    y *= -1;
    if (x < 0.5) x *= -1;
    let magnitude = Math.sqrt(x * x + y * y);
    let ballVelX = speed * x / magnitude;
    let ballVelY = speed * y / magnitude;
    if(ballVelX > ballVelY) return [ballVelY, ballVelX];
    return [ballVelX, ballVelY];
}


class Ball {
    constructor(ctx, boardWidth, boardHeight, color, radius, speed, paddle, blocks, lives) {
        this.ctx = ctx;
        this.width = boardWidth;
        this.height = boardHeight;
        this.initialSpeed = speed;
        this.paddle = paddle;
        this.blocks = blocks;
        this.blockCounter = this.blocks.length;
        this.velocity = initialDirection(speed);
        this.radius = radius;
        this.color = color;
        this.position = this.getBallResetPosition();
        this.lives = lives;

    }

    onBottomWall() {
        return this.position[1] > this.height;
    }

    onTopWall() {
        return this.position[1] <= this.radius;
    }

    onSideWalls() {
        if (this.position[0] <= this.radius) {
            return true;
        } else if (this.position[0] >= this.width - this.radius) {
            return true;
        }
    }

    getBallResetPosition() {
        return [this.paddle.position[0] + this.paddle.width / 2, this.paddle.position[1] - this.radius - 1]; // -1 for not touch it
    }

    inXRange(bl) {
        return (this.position[0] + this.radius >= bl.position[0] && this.position[0] - this.radius <= bl.position[0] + bl.width);
    }

    inYRange(bl) {
        return this.position[1] + this.radius >= bl.position[1] && this.position[1] - this.radius <= bl.position[1] + bl.height;
    }

    paddleDetect() {
        if (this.inYRange(this.paddle) && this.inXRange(this.paddle)) {
            this.velocity[1] *= -1
        }
    }

    collisionDetect() {

        this.blocks.forEach((bl, i) => {
            if (!bl.visible) return;
            if (this.inXRange(bl) && this.inYRange(bl)) {
                this.velocity[1] *= -1;
                bl.visible = false;
                this.blockCounter -= 1;
            }
        });
    }

    shortenPaddle(percentage) {
        if (this.paddle.width > 5) this.paddle.width *= (1 - percentage / 100);
    }

    resetBall() {
        this.velocity[1] *= -1;
        this.position = this.getBallResetPosition();
        this.gradient();
        this.velocity = initialDirection(this.initialSpeed);
    }

    move() {

        this.paddleDetect();
        this.collisionDetect();

        if (this.onSideWalls()) this.velocity[0] *= -1;
        if (this.onTopWall()) this.velocity[1] *= -1;
        if (this.onBottomWall()) {
            this.lives -= 1;
            this.shortenPaddle(10);
            console.log('Lives: ', this.lives)
            this.resetBall();
            return;

        };
        this.position[0] += this.velocity[0];
        this.position[1] += this.velocity[1];
    }

    gradient() {

        // Create gradient
        let grd = this.ctx.createRadialGradient(
            this.position[0],
            this.position[1],
            this.radius / 7.5,
            this.position[0],
            this.position[1],
            this.radius + this.radius / 5
        );

        //// Gradients
        // grd.addColorStop(0.1,"#e59494");
        // grd.addColorStop(0.1,"#dd7373");
        // grd.addColorStop(0.1,"#d34a4a");
        grd.addColorStop(0.1, "#b12b2b");
        grd.addColorStop(1, "#4a1212");

        // Fill with gradient
        this.ctx.beginPath();
        this.ctx.arc(this.position[0], this.position[1], this.radius, 0, 2 * Math.PI, false);
        this.ctx.fillStyle = grd;
        this.ctx.strokeStyle = 'rgba(0, 0, 0, 0.7)';
        this.ctx.lineWidth = this.radius / 10;
        this.ctx.stroke();
        //ctx.fillStyle = 'red'
        this.ctx.fill();

        // var grad1 = ctx.createRadialGradient(this.position[0],this.position[1], 0, 50, 50, 50);
        // grad1.addColorStop(0, 'rgba(255, 252, 0, 1)');
        // grad1.addColorStop(1, 'rgba(68, 205, 37, 1)');

        // this.ctx.fillStyle = grad1;
    }

    render() {
        this.ctx.beginPath();
        this.ctx.arc(this.position[0], this.position[1], this.radius, 0, 2 * Math.PI, false);
        this.ctx.fillStyle = this.color;
        // this.ctx.strokeStyle = 'rgba(0, 0, 0, 0.6)';
        // this.ctx.lineWidth = 0.1;
        // this.ctx.stroke();
        this.ctx.fill();
    }
}