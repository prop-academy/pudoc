// Will not work
// console.log(a);
// let a = 9;

// Will work, but dangerous
console.log(b);
var b = 6;


// let is always scoped

let i = 12;
function x() {
    console.log(i);
    if (true) {
        let i = 10;
        console.log(i);
    }
    console.log(i);
    return ""
}
// x();


var j = 25438583893;
function y() {
    console.log(j)
    for (let j; j < 10; j++) {
        console.log(j);
    }
    console.log(j);
    return ""
}

y();    // will print 25438583893, then 25438583893 again, let is purely inside the for loop (always block-scoped)