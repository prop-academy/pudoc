function myEach(arr, callback) {
    for(let i=0; i < arr.length; i++){
        callback(arr[i], i);
    }
}

let myList = [1,2,3,5]

myEach(myList, (el, i) => console.log(el, i));