// invert an object: It returns a new object with the values of the passed object as properties and the keys as values of those properties.
function invert(obj) { 
    let keys = Object.keys(obj);
    let newObj = {};
    keys.forEach(key => {
        newObj[obj[key]] = key; 
    })
    return newObj;
}

console.log(invert({ a: 3, b: 2 })); // { 2: 'b', 3: 'a' }