# Why use alpine Linux

- [Small, Simple, and Secure: Alpine Linux under the Microscope by Natanael Copa](https://www.youtube.com/watch?v=sIG2P9k6EjA)

# Docker in production

- [Docker in production by Bret Fischer](https://www.youtube.com/watch?v=6jT83lT6TU8)
    - Docker File is the basis. Use the os you are familiar with and start with that (alpine-linux later)
    - Document Docker image if complex
    - Prepare Docker image for orchestration
    - A single Dockerfile for all the environments (otherwise that's an anti-pattern)
    - Volumes for stuff like logs that should persist are often forgotten in Dockerfiles.
    - Log stuff to stdout and stderr and collect it with orchestration tool.
    - Dont use tag latest - not in dev and NOT in production.
    - Docker is reliant heavily on kernel version (much more than os version.). Use a 4.x kernel.

# Overviews

- https://github.com/cncf/landscape

# Container Design
- [Building effective images by Abby Fuller, AWS](https://www.youtube.com/watch?v=vlS5EiapiII)

# Security

- [GID and UID and docker containers](https://medium.com/@mccode/understanding-how-uid-and-gid-work-in-docker-containers-c37a01d01cf)