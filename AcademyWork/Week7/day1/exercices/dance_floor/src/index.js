// import './styles/style.css'
import React from 'react'
import ReactDOM from 'react-dom'
import App from './containers/app/App.js'

ReactDOM.render(
    <App />,
    document.getElementById('app')
)
