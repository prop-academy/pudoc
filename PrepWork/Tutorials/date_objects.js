// By default, JavaScript will use the browser's time zone and display a date as a full text string:
// Zero time is January 01, 1970 00:00:00 UTC.


var d = new Date();

// as ISO String
var d = new Date("2015-03-25");
// ISO date with datetime
var d = new Date("2015-03-25T12:00:00Z");

// Date and time is separated with a capital T.
// UTC time is defined with a capital letter Z.
// If you want to modify the time relative to UTC, remove the Z and add +HH:MM or -HH:MM instead:

var d = new Date("2015-03-25T12:00:00-06:30");
// month and year
var d = new Date("2015-03");
// year only
var d = new Date("2015");
// The computed date will be relative to your time zone.
// Depending on your time zone, the result above will vary between March 24 and March 25.

 /*
new Date()
new Date(year, month, day, hours, minutes, seconds, milliseconds)
new Date(milliseconds)
new Date(date string)

There are generally 3 types of JavaScript date input formats:
  - ISO Date 	"2015-03-25" (The International Standard)
  - Short Date 	"03/25/2015"
  - Long Date 	"Mar 25 2015" or "25 Mar 2015"
 */

 // JavaScript counts months from 0 to 11
var d = new Date(2018, 11, 24, 10, 33, 30, 0);
var d = new Date(2018, 11, 24, 10, 33);

// You have to specify year and month at minimum
// The following will be interpreteted as milliseconds
var d = new Date(2018);

// Previous century: One and two digit years will be interpreted as 19xx:
var d = new Date(99, 11, 24);

// New Date from datestring
var d = new Date("October 13, 2014 11:13:00");

// Some methods

// The toUTCString() method converts a date to a UTC string (a date display standard).
console.log(d.toUTCString());
// The toDateString() method converts a date to a more readable format:
console.log(d.toDateString());

// Short dates
// Short dates are written with an "MM/DD/YYYY" syntax like this:
var d = new Date("03/25/2015");
// The behavior of "YYYY/MM/DD" is undefined.
// The behavior of  "DD-MM-YYYY" is also undefined.

// JavaScript Long Dates
// Long dates are most often written with a "MMM DD YYYY" syntax like this:
var d = new Date("Mar 25 2015");

// Month and day can be in any order:
var d = new Date("25 Mar 2015");

// And, month can be written in full (January), or abbreviated (Jan):
var d = new Date("January 25 2015");

// Commas are ignored. Names are case insensitive:
var d = new Date("JANUARY, 25, 2015");


// Date.parse()
 var msec = Date.parse("March 21, 2012");
 console.log(msec);

// OVERVIEW DATE methods

/*
These methods can be used for getting information from a date object:
Method 	Description
getFullYear() 	Get the year as a four digit number (yyyy)
getMonth() 	Get the month as a number (0-11)
getDate() 	Get the day as a number (1-31)
getHours() 	Get the hour (0-23)
getMinutes() 	Get the minute (0-59)
getSeconds() 	Get the second (0-59)
getMilliseconds() 	Get the millisecond (0-999)
getTime() 	Get the time (milliseconds since January 1, 1970)
getDay() 	Get the weekday as a number (0-6)
Date.now() 	Get the time. ECMAScript 5.

UTC date methods are used for working with UTC dates (Universal Time Zone dates):
Method 	Description
getUTCDate() 	Same as getDate(), but returns the UTC date
getUTCDay() 	Same as getDay(), but returns the UTC day
getUTCFullYear() 	Same as getFullYear(), but returns the UTC year
getUTCHours() 	Same as getHours(), but returns the UTC hour
getUTCMilliseconds() 	Same as getMilliseconds(), but returns the UTC milliseconds
getUTCMinutes() 	Same as getMinutes(), but returns the UTC minutes
getUTCMonth() 	Same as getMonth(), but returns the UTC month
getUTCSeconds() 	Same as getSeconds(), but returns the UTC seconds

*/
