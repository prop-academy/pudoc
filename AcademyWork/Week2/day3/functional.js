const array = [1,2,3];

// declarative way
function multi2(array){
    let r = []
    for(let i; i < array.length; i++){
        r.push(array[i]);
    }
    return r;
}

// functional programming --> no for loops

const multi3 = array => map(num => {return num * 2;})
