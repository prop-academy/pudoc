# Game Bricks in the Wall

## Problems along the ways (using Firefox)

- Changed only the starting velocity with function `initialDirection(speed)` and the ball will not bounce off the side walls ?!. ==> Floating point issues.
- A big scaling factor is not helping to fine tune the game. Rather leave it with 1.
- Never use === in comparisons of position.