/* You need to create a program that will translate from English to Pig Latin. Pig Latin takes the first consonant
 (or consonant cluster) of an English word, moves it to the end of the word and suffixes an “ay”. If a word begins
  with a vowel you just add “way” to the end. It might not be obvious but you need to remove all the consonants up to
   the first vowel in case the word does not start with a vowel.
 */


// if('a' in ['a', 'b', 'c']) does not work properly, use include or indexof or find!!!!

function findFirstVowelIndex(str) {
    const vowels = ['a', 'e', 'i', 'o', 'u'];
    for(let index in str){
        // check every letter of the string if it is
        let letter = str[index];
        if(vowels.indexOf(letter) !== -1 ){
            return index;
        }
    }
    return -1;
}

function translatePigLatin(str) {
    firstVowelIndex = findFirstVowelIndex(str);
    let pigSay = str.slice(firstVowelIndex) + str.slice(0, firstVowelIndex) + 'ay'
    console.log(pigSay);
    return pigSay;
}



translatePigLatin("glove"); // oveglay
translatePigLatin("pig"); // igpay
translatePigLatin("apple"); // igpay
