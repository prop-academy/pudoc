// Math is not a function object, so the below idea does not work

// class MyMath extends Math {
//     constructor(...args){
//         super(...args)
//     }

//     randomInt(max, min=0) {
//         let num = this.floor(this.random() * (max - min + 1)) + min;
//         return num;
//     }
// }

// MyMath.randomInt(4);

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math

MyMath = Math;

MyMath.randomInt = function (max, min = 0) {
    let num = this.floor(this.random() * (max - min + 1)) + min;
    return num;
}

console.log(MyMath.randomInt(50,34));


