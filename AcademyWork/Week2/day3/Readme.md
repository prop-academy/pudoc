# Intro Node

- use `module.exports = MyClass` and in another file `const student = require('./students.js')`


# Functional Programming

- **Higher Order Functions**: e.g. callback functions, that are passed in other functions.
- **Pure function**: When the function is not dependant on global variables, which is generall a bad idea.
- also do not change the original objects if this is not intended.

## function piping

- beware of using `forEach` since it does not return;
- don't forget `return` in simple callback functions in `Array.map()`.

## Dangerous

- `splice` changes the array, so it is not pure.
- `reverse` changes the array, too.

### Examples

- This will create a new object always. A **shallow copy** is not inheriting the prototype methods, just the values.

    ```javascript

    function createObj(obj){
        const newObj = {...obj} // ES6, shallow copy
        newObj.active = true;
        return newObj;
        const newObj = Object.assign({}, obj)} // same as Object.create(obj.prototype), dont use this anymore

    ```

- Spread Operator: 0th and 1st element of 
     ```javascript

    let list = [1,2,3];    
    [x,y, ...newArr] = list;    // newArr = [3], x=1, y=2;
    const reverseArray = ([x, ...arg]) => x !== undefined ? [...reverseArray(arg), x] : [];

    ```

