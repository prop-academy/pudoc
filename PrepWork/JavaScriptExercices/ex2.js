// Write a method that takes an integer n in; it should return n*(n-1)*(n-2)*...*2*1. 
// Assume n >= 0.

function factorial(int) {
    if (!int) return 1;
    if (int < 0) return 'n must be > or = to 0'

    let i = 2;
    let result = 1;
    for (i; i <= int; i++) {
        result *= i;
    }
    return result;
}

console.log(factorial(5));
console.log(factorial(4));
console.log(factorial(0));
console.log(factorial(-1));