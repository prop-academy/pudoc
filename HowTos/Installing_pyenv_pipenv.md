# How-To install pyenv and pipenv

## Installing pyenv

- [Install dependencies](https://github.com/pyenv/pyenv/wiki/Common-build-problems)
- [Install Pyenv](https://github.com/pyenv/pyenv-installer)

Follow the instruction on the command line and edit your `.bash_rc` or `.bashrc`.

### Install a python version

Update, download python 3.7 and set it this version as the global version.

```bash
pyenv update
pyenv install 3.7.0
pyenv global 3.7.0
```

### Install packages for pipenv
```bash

sudo apt install python3-pip python3-setuptools

```
Add `.local/bin` directory to path

```bash
cat >>~/.bash_profile <<EOD
export PATH="\$HOME/.local/bin:\$PATH"
EOD
```

### Install pipenv

Source: [Official Homepage of pipenv](https://pipenv.readthedocs.io/en/latest/install/#installing-pipenv)

### Create virtual environment


### Workflow

The first thing to do after installing pyenv first and then pipenv

1. Create a project, `mkdir myproject && cd myproject`.
2. Create a virtual environment with a downloaded python version. The output should look something like this:
    ```bash
    user@big-one:~$ pipenv --version
    pipenv, version 2018.11.26

    user@big-one:~$ cd myproject/ && pipenv --python 3.7.0

    Creating a virtualenv for this project…
    Pipfile: /home/user/myproject/Pipfile
    Using /home/user/.pyenv/versions/venv/bin/python (3.7.0) to create virtualenv…
    ⠋ Creating virtual environment...Using base prefix '/home/user/.pyenv/versions/3.7.0'
    New python executable in /home/user/.local/share/virtualenvs/myproject-WPyNl_0Y/bin/python
    Installing setuptools, pip, wheel...
    done.
    Running virtualenv with interpreter /home/user/.pyenv/versions/venv/bin/python

    ✔ Successfully created virtual environment! 
    Virtualenv location: /home/user/.local/share/virtualenvs/myproject-WPyNl_0Y
    Creating a Pipfile for this project… 
    ```

3. Pycharm supports pipenv but it does not see the pyenv's base python versions, it just looks at the directory `/usr/bin`. The magic happens in Pycharm when you go to the Terminal inside Pycharm and type `pipenv shell`. Then in your shell the virtual environment is activated and suddenly pycharm can see it. Click *use pipenv executable* or go to Settings (`Ctrl+Alt+S`) and set the interpreter.

4. Installing packages manually, be sure that the virtual env is activated (`pipenv shell`) and then install them with `pipenv install package_name`.

### Configure Pycharm to use pipenv

- Source: https://www.jetbrains.com/help/pycharm/pipenv.html


