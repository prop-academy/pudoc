// Write a method that will take in a number of minutes, and returns a string that formats the number into hours:minutes.

function timeConversion(minutes) {
    if (!Number.isInteger(minutes)) return 'Please enter an integer!'
    if (minutes > 99 * 60 + 59) return 'Please enter a smaller amount of minutes!'

    let fullHours = Math.floor(minutes / 60).toString();
    let remainingMinutes = (minutes % 60).toString();

    if (fullHours.length === 1) fullHours = '0' + fullHours;
    if (remainingMinutes.length === 1) remainingMinutes = '0' + remainingMinutes;

    return `${fullHours}:${remainingMinutes}`
}

console.log(timeConversion(155))
console.log(timeConversion(61))
console.log(timeConversion(60))
console.log(timeConversion(59))
