function revString(str) {
    return str.split("").reverse().join('')
}

class Person {
    constructor(firstName, lastName, isAlive = true) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.isAlive = isAlive;
    }

    greeting() {
        console.log(`Hello this is ${this.firstName} ${this.lastName}.`)
    }

}

class Writer extends Person {
    constructor(firstName, lastName, isAlive) {
        super(firstName, lastName, isAlive)

        this.pseudonym = this.getPseudonym()
    }

    getPseudonym() {
        return (revString(this.firstName) + revString(this.lastName));
    }

    signBook() {
        console.log(`Happy reading! \n\nby ${this.pseudonym}`)
    }
}

writer = new Writer('Hannes', 'Bernstein')
writer.signBook();

class Developer extends Person {
    constructor(firstName, lastName, isAlive, codename) {
        super(firstName, lastName, isAlive)
        this.codename = codename;
    }

    impress() {
        let line = `00000000000000000000000000\n`
        console.log(line.repeat(5));
        console.log(this.codename);
    }
}

dev = new Developer('John', 'Doe', isAlive=true, codename = 'Orion');
dev.impress();

class Singer extends Person {
    constructor(firstName, lastName, isAlive, melody) {
        super(firstName, lastName, isAlive)
        this.melody = melody;
        this.artisticName = `Fancy${this.firstName}${this.lastName}`;
    }

    sing() {
        console.log(this.melody.repeat(3))
    }

}

singer = new Singer('Michael', 'Jackson', isAlive=false, melody='Ou')
singer.sing();

class JuniorDeveloper extends Developer {

    constructor(firstName, lastName, isAlive, codename, isStruggling=true) {
        super(firstName, lastName, isAlive, codename)
        this.isStruggling = isStruggling;
        this.artisticName = `Fancy${this.firstName}${this.lastName}`;
    }

    complain(){
        console.log(this.codename.toUpperCase())
    }

    workHard(){
        let message = `${this.codename} is working hard\n`
        console.log(message.repeat(10));
    }

}

jundev = new JuniorDeveloper('Bobby', 'Jay', isAlive=true, codename='BillyBob')
// jundev.complain();
// jundev.workHard();

const laurent = new Developer('Laurent', 'Hoxhaj', 'Ping Pong King');

/* Questions

    1. What’s the __proto__ of Person?

        is the native Function object

    2. What’s the prototype of Writer?

        is the native javascript Object with the constructor beeing the Writer function

    3. Does laurent have a __proto__ or a prototype? Why?

        laurent is an instance, so it has a __proto__ but not a prototype. The Developer.prototype becomes laurant.__proto___ after instantiation

    4. What is the difference between __proto__ and prototype?

*/

