// JavaScript will actually create an Error object with two properties: name and message.
/*
Error Name    	Description
-------------------------------------------------------------
EvalError	      An error has occurred in the eval() function
Newer versions of JavaScript does not throw any EvalError. Use SyntaxError instead.

RangeError	    A number "out of range" has occurred
ReferenceError	An illegal reference has occurred
SyntaxError	    A syntax error has occurred
TypeError	      A type error has occurred
URIError	     An error in encodeURI() has occurred
*/

// An exampple
function myFunction() {
  var message, x;
  message = document.getElementById("p01");
  message.innerHTML = "";
  x = document.getElementById("demo").value;
  try {
    if(x == "") throw "empty";
    if(isNaN(x)) throw "not a number";
    x = Number(x);
    if(x < 5) throw "too low";
    if(x > 10) throw "too high";
  }
  catch(err) {
    message.innerHTML = "Input is " + err;
  }
}

/*
The finally Statement

The finally statement lets you execute code, after try and catch, regardless of the result:
try {
  Block of code to try
}
catch(err) {
  Block of code to handle errors
}
finally {
  Block of code to be executed regardless of the try / catch result
}
*/
//  SyntaxError
try {
  eval("alert('Hello)");   // Missing ' will produce an error
}
catch(err) {
  console.log(err.name);
}

// TypeError
var num = 1;
try {
  num.toUpperCase();   // You cannot convert a number to upper case
}
catch(err) {
  console.log(err.name);
}

//  URIError
try {
  decodeURI("%%%");   // You cannot URI decode percent signs
}
catch(err) {
  console.log(err.name);
}

// ReferenceError
var x;
try {
  x = y + 1;   // y cannot be referenced (used)
}
catch(err) {
  console.log(err.name);
}

// RangeError
var num = 1;
try {
  num.toPrecision(500);   // A number cannot have 500 significant digits
}
catch(err) {
  console.log(err.name);
}
