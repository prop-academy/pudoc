/*
Scope

  GLOBAL: can be accessed in all scripts on the webpage
*/

/*Automatically Global
If you assign a value to a variable that has not been declared, it will automatically become a GLOBAL variable.
This code example will declare a global variable carName, even if the value is assigned inside a function.*/

myFunction();

// A variable var declared outside a function, becomes GLOBAL.

var test = 'testing';   // will be global

// code here can use carName

function myFunction() {
  carName = "Volvo";
}

/*
Global Variables in HTML

With JavaScript, the global scope is the complete JavaScript environment.

In HTML, the global scope is the window object. All global variables belong to the window object.
*/
var carName = "Volvo";

// code here can use window.carName
