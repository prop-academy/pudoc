# [Learning course from Mozilla Developers page](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/)

This Readme lists an overview of the completet material from the source above linking to the respective files. 

## Introducing JavaScript Objects

- [Object Prototypes](prototypes.js)
- [Object Prototypes Example](oojs_class_further_exercices.js)
- [Object Inheritance with call-method, use Inspector Console of the browser](oojs-class-inheritance-start.html)
- [Ecma Script 2015 classes](ES2015_classes.js)
- Bouncing balls