import './styles/style.css'
import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/app/index.js'

ReactDOM.render(

    // React.createElement('h1', null, 'This is the content, the second is styles of child elements'),
    <App />,
    document.getElementById('root')
)
