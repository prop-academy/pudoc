# Node CLI

- `Tab`: get normal autocomplete
- `Shift+Tab`: get autocomplete options like in an IDE

# VS Code

- `Alt+ArrowUP/Down`: moves the current line up or down
- `Ctrl+Shift+Arrow`: Multicursor like in Atom
- `Home/End`: Jump to beginnung or end of line

# Linux

- tired of typing docker-compose? make an `alias d-c="docker-compose"` and add that to the file `~/.bash_aliases`, Create if does not exist. You can find the reference to this file in `~/.bash_rc` or `~/.bashrc` depending on linux os. From now on type `d-c`. Go crazy with aliases!

# Markdown

- [Special Gitlab References](https://blue.cse.buffalo.edu/gitlab/help/markdown/markdown.md#special-gitlab-references)
- [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

# Data Science?

- plenty of open api's: https://github.com/toddmotto/public-apis

# JS Terminal in Browser??

- https://terminal.jcubic.pl/