# React CLI Setup

- developed by facebook people, it's a simple git repo.
- installation with `npx i -g create-react-app`: This clones the git repository, so you have to delete `.git` after downloading.
- delete stuff not needed, keep the `serviceworker`.

## Coupling of state and e.g Input-Field

Given the input of an Input Field updated the state.

- Input field updates state.
- Updating the state will trigger render function.
- Input field is rendered to DOM. It get's it's value from the state.

## Props

- `static defaultProps` can be used to define default props for a component, e.g. when a user is not logged in
- The prop-types package can be used to check the passed props to output a warning on the console, that the properties have to
be passed.