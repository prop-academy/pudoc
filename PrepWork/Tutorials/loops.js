// FOR LOOPS

var fruits, text, fLen, i;
fruits = ["Banana", "Orange", "Apple", "Mango"];
fLen = fruits.length;

text = "<ul>";
for (i = 0; i < fLen; i++) {
  text += "<li>" + fruits[i] + "</li>";
}
text += "</ul>";

// With an array methods

var fruits, text;
fruits = ["Banana", "Orange", "Apple", "Mango"];

text = "<ul>";
fruits.forEach(myFunction);
text += "</ul>";

function myFunction(value) {
  text += "<li>" + value + "</li>";
}

var fruits = ["Apple", "Banana", "Orange"];
for (x in fruits) {
  console.log(x);
}

for (x of fruits) {
  // Prints the values instead of indexes
  console.log(x);
}

// Loop through objects values
// are objects ordered?
var person = {fname:"John", lname:"Doe", age:25};

var text = "";
var x;
for (x in person) {
  text += person[x];
} // outputs JohnDoe25


// Loop that runs as long as i is less than 10
var i = 0;
while (i < 10){
    console.log(i)
    i++
};


const obj = {a:1, b:2, c:3};

for (let i in obj) console.log("i: ", i);
for (let i of obj) console.log("i: ", i);  // does not work, of in objects is not possible, only for arrays

