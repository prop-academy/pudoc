// Write a method that takes a string and returns true if it is a palindrome.
//  A palindrome is a string that is the same whether written backward or forward. 
//  Assume that there are no spaces; only lowercase letters will be given.

function palindrome(string) {
    return string === string.split('').reverse().join('')
}

console.log(palindrome('ABBA'))
console.log(palindrome('AbbA'))
console.log(palindrome('abcd'))