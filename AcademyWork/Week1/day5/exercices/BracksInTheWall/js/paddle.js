'use strict';

class Paddle {
    constructor(ctx, canvasWidth, canvasHeight, keyLeft, keyRight, paddleVel, position, paddleWidth, paddleHeight, color) {
        this.ctx = ctx;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.keyLeft = keyLeft || 'ArrowLeft';
        this.keyRight = keyRight || 'ArrowRight';
        this.paddleVelocity = paddleVel;

        this.width = paddleWidth;
        this.height = paddleHeight;
        this.position = position;
        this.color = color;
        document.addEventListener('keydown', event => { this.move(event); });
    }

    move(e) {
        if (e.key === this.keyLeft && this.position[0] > 0) {
            // console.log('move paddle left triggered!!')
            this.position[0] -= this.paddleVelocity;
        }
        if (e.key === this.keyRight && this.position[0] + this.width < this.canvasWidth) {
            // console.log('move paddle right triggered!!')
            this.position[0] += this.paddleVelocity;
        }
    }

    render() {
        this.ctx.beginPath()
        this.ctx.fillStyle = this.color
        this.ctx.rect(this.position[0], this.position[1], this.width, this.height);
        this.ctx.fill();
    }
}
