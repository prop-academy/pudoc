/* Take 2 strings a and b including only letters from a to z. Return a new string that only 
contains each letter from a and/or b once and that is sorted. */

function longest(...args) {

    let hugeString = "";
    args.forEach(elem => hugeString += elem);

    let hugeList = hugeString.split('');

    let r = [];
    for (arg of hugeList) {
        if (r.indexOf(arg) === -1) r.push(arg);
    }
    let finalString = r.sort().join('');
    console.log(finalString);
    return finalString;
}

longest('abcccaa', 'acddddffzzz') // => 'abcdfz'

a = 'xyaabbbccccdefww'
b = 'xxxxyyyyabklmopq'
longest(a, b) // => 'abcdefklmopqwxy'

a = 'abcdefghijklmnopqrstuvwxyz'
longest(a, a) // => 'abcdefghijklmnopqrstuvwxyz'
