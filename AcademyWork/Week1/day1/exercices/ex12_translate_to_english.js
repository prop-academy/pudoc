/* This problem is very straight forward, you will get a string that will represent a sentence in binary code,
 and you need to translate that into words. There is not direct way to do this so you will have to translate twice.

Useful - Check these two on MDN

    String.fromCharCode() - Converts unicode values into characters
    String.prototype.charCodeAt() returns an integer between 0 and 65535 representing the UTF-16 code unit at the given index.
    */

function toEnglish(binaryString) {
    let list = binaryString.split(' ');
    let char_list = list.map(bin => String.fromCharCode(parseInt(bin, 2)));

    result = char_list.join('')
    return result;
}

console.log(
    toEnglish(
        "01000001 01110010 01100101 01101110 00100111 01110100 00100000 01100010 01101111 01101110 01100110 01101001 01110010 01100101 01110011 00100000 01100110 01110101 01101110 00100001 00111111"
    ))