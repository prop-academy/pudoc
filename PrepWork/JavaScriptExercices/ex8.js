// Write a method that takes a string in and returns true if the letter “z”
// appears within three letters after an “a”. You may assume that the string contains only lowercase letters.

function nearby_az(string) {
    let howNear = 3;
    let z_ind = string.indexOf('z');
    let a_ind = string.indexOf('a');
    if (a_ind === -1) return false;
    if (z_ind === -1) return false;

    // recursive function
    function search_z(start_ind) {
        if (start_ind > string.length) return false;
        // console.log(`Starting with index ${start_ind} and string ${string[start_ind]}`)
        let i = 0;
        for (i; i < howNear; i++) {

            let l = string[start_ind + i + 1]
            // console.log('-', l)

            if (l === 'z') return true;
        }
        // start_ind + 1 important, else overflow!
        let next_a_ind = string.indexOf('a', start_ind + 1);
        if (next_a_ind === -1) return false;
        search_z(next_a_ind);
    }

    result = search_z(a_ind);
    return result;

}

console.log(nearby_az("abbbz"))// false
console.log(nearby_az("abz")) // true
console.log(nearby_az("abcz")) // true
console.log(nearby_az("abba"))// false
