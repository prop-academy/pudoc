# Networking, fetching and life cycle methods


## Lifecycle methods

- a react component mounts and dismounts.
- fetching in the constructor is not a good practice. Do it for example in
`componentDidMount`
- renders is called when state or passed props change.
- `fetch` is a function that comes from the browser and is not part of node.
You can use isomorphic-unfetch or something like that.
- A promise is cind of a future value. With promises you can interact with
the event loop of JavaScript.
- the promise has a `.then` and a `.catch` handler method to get the data
or handle the exception of the fetch.
- the `.catch` at the end of the chain, will catch all the errors in the chain before.
- You can chain `.then` handlers.