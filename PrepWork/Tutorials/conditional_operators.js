// Conditional (ternary) operators
var age = n;
var voteable = (age < 18) ? "Too young": "Old enough";
alert(voteable);

/*When comparing two strings, "2" will be greater than "12", because (alphabetically) 1 is less than 2.

To secure a proper result, variables should be converted to the proper type before comparison:*/
age = Number(age);
if (isNaN(age)) {
  voteable = "Input is not a number";
} else {
  voteable = (age < 18) ? "Too young" : "Old enough";
}
