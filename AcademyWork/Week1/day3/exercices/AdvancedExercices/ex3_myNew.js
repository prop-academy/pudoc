/* Define myNew function that simulates the new keyword.

You have already seen an implementation of myNew in the notes 
but it doesn’t handle parameter. Your function should be able to handle parameters.

new does 4 things

1. creates a new object
2. binds this parameters from blueprint
3. attach methods to the prototype, since it's not an instance it goes to constructor.prototype
4. Return the object.

const newObj = {}; // 1.
      fn.call(newObj); // 2.
      Object.setPrototypeOf(newObj, fn.prototype); // 3.

      return newObj; // 4.

*/

const Tester = function () {
    this.name = 'test'
}
Tester.prototype.testmethod = function () { console.log('test passed') }

/* ES6 definition will not work, the code below with myNew will raise Error: object.constructor can just be invoked with "new"

class Tester {
    constructor(){
        this.name = 'test'
    }
    testmethod() { console.log('test passed') }
} */

function myNew(objBlueprint, ...arguments) {
    let robj = {};
    // .call(this, ...arguments)
    objBlueprint.call(robj, ...arguments)
    Object.setPrototypeOf(robj, objBlueprint.prototype) // robj.constructor.prototype = objBlueprint.prototype
    return robj
}

let newString = myNew(Tester)
console.log(newString);