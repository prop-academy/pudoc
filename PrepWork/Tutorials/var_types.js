// GENERAL
/*
    - All JavaScript data types have a valueOf() and a toString() method.

*/
// Primitive variables

typeof "John"              // Returns "string"
typeof 3.14                // Returns "number"
typeof true                // Returns "boolean"
typeof false               // Returns "boolean"
typeof x                   // Returns "undefined" (if x has no value)

// Complex data: objects or functions
typeof {name:'John', age:34}; // Returns "object"
typeof [1,2,3,4]   ;          // Returns "object", arrays are objects in javascript
typeof null;                // Returns "object"
typeof NaN                    // Returns "number"
typeof function myFunc(){}   // Returns "function"
typeof new Date()             // Returns "object"

// Create an object
var person = {
  firstName: "John",
  lastName : "Doe",
  id       : 5566,
  fullName : function() {
    return this.firstName + " " + this.lastName;
  }
};
typeof person;
// Accessing properties
person.firstName;
person.fullName();
person['age'];
person = null;        // Now value is null, but type is still an object
person = undefined;   // Now both value and type is undefined

// Objects can not be compared, it will always return false
var x = new String("John");
var y = new String("John");

x == y;

// Create an array, always put the last comma
var cars = ["Saab", "Volvo", "BMW",];
typeof cars
// array can contain other objects


// Declared, but undefined variable, type is undefined
var car;
typeof car
car = 'somestring'
typeof car
car = undefined;        // Value is undefined, type is undefined
typeof car

// Difference between null and undefined
typeof undefined           // undefined
typeof null                // object

null === undefined         // false
null == undefined          // true

// When a JavaScript variable is declared with the keyword "new", the variable is created as an object:
var x = new String();        // Declares x as a String object
var y = new Number();        // Declares y as a Number object
var z = new Boolean();       // Declares z as a Boolean object
// Avoid String, Number, and Boolean objects. They complicate your code and slow down execution speed.
