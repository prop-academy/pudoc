
function findSmallest(L) {
    let smallest = Infinity;
    let smallestInd = -1;

    L.forEach((element, index) => {
        if (element < smallest) {
            smallestInd = index;
            smallest = element;
        }
    });
    return smallestInd;
}


function selectionSort(L) {
    let L_sorted = [];
    while (L.length > 1) {
        let minInd = findSmallest(L);
        L_sorted.push(L.splice(minInd, 1)[0]);
    }

    L_sorted.push(L[0]);
    return L_sorted;
}

console.log(selectionSort([52, 3, 53, 6, 70, 18, 9]));
console.log(selectionSort([5, 3, 5, 6, 7, 18, 9]));