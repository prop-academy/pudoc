import random
import time

# Did just one class handling all the functionality and doing a function mapping based in a string operator symbol
class MathQuestion:

    allowed_operators = ['+', '-', '/', '*']
    __operations = {
        '+': lambda x, y: x + y,
        '-': lambda x, y: x - y,
        '/': lambda x, y: x / y,
        '*': lambda x, y: x * y,
    }

    def __init__(self, arg1=None, arg2=None, operator_symbol = ''):

        self.arg1 = arg1
        self.arg2 = arg2
        self.operator_symbol = self.__check_operator(operator_symbol)

    def __check_operator(self, operator_symbol):
        assert operator_symbol in self.allowed_operators
        return operator_symbol

    @property
    def text(self):
        return f"{str(self.arg1)} {self.operator_symbol} {str(self.arg2)} ?"

    @property
    def answer(self):
        return self.__operations[self.operator_symbol](self.arg1, self.arg2)


class TheQuiz:

    num_of_questions = 15

    def __init__(self):
        self.generate_questions()

    def random_num(self, max):
        return random.randint(0, max)

    def generate_questions(self):
        operators = MathQuestion.allowed_operators
        op = operators[self.random_num(len(operators)-1)]
        self.questions =  [MathQuestion(self.random_num(10), self.random_num(10), op) for i in range(self.num_of_questions)]

    def start(self):
        start_time = time.perf_counter()
        time_per_question = []
        correct_answers = 0
        waiting_time = 1

        print('Welcome to this easy Math quiz\n Just answer questions by typing in your solution and hit enter.\n')

        for question in self.questions:
            start_question_time = time.perf_counter()

            guess = input('Question: ' + question.text + ' ')
            time_per_question.append(start_question_time - time.perf_counter())

            if int(guess) == question.answer:
                print('Correct')
                correct_answers += 1
            else:
                print(f'Wrong. Correct answer is {question.answer}.')

            time.sleep(waiting_time)

        end_time = time.perf_counter()

        final_message = f"""
        !!Congratulations!!
         You finished the quiz in {end_time-start_time} seconds.
         You answered {correct_answers} of {self.num_of_questions} correctly."""

        print(final_message)


if __name__ == '__main__':

    quiz = TheQuiz()
    quiz.start()