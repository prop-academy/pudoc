// Objects are not exaclty similar to python  classes (arrays are also
// obejcts)

// This in an object

var person = {
  firstName: "John",
  lastName : "Doe",
  id       : 5566,
  fullName : function() {
    return this.firstName + " " + this.lastName;
  }


/*
In a method, this refers to the owner object.
Alone, this refers to the global object.
In a function, this refers to the global object.
In a function, in strict mode, this is undefined.
In an event, this refers to the element that received the event.
Methods like call(), and apply() can refer this to any object.
*/
// In a browser window the Global object is [object Window]:

var x = this;

// this in a Function (Default)

/*
In a JavaScript function, the owner of the function is the default binding for this.
So, in a function, this refers to the Global object [object Window].*/

function myFunction() {
  return this;
}

// this in a Function (Strict)

/*
JavaScript strict mode does not allow default binding.
So, when used in a function, in strict mode, this is undefined.
*/

"use strict";
function myFunction() {
  return this;
}

// this in Event Handlers

/*
In HTML event handlers, this refers to the HTML element that received the event:
*/

<button onclick="this.style.display='none'">
  Click to Remove Me!
</button>

// Explicit Function Binding

/*
The call() and apply() methods are predefined JavaScript methods.
They can both be used to call an object method with another object as argument.
In this example, when calling person1.fullName with person2 as argument, this will refer to person2, even if it is a method of person1:
*/
var person1 = {
  fullName: function() {
    return this.firstName + " " + this.lastName;
  }
}
var person2 = {
  firstName:"John",
  lastName: "Doe",
}
person1.fullName.call(person2);  // Will return "John Doe"
