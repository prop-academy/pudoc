// Write a method that will take a string as input, and return a new string with the same letters in reverse order.

function reverse(string) {
    l = string.split('')
    return l.reverse().join('');
}

console.log(reverse("Propulsion Academy"))
console.log(reverse("Hello"))
console.log(reverse("abcd"))
