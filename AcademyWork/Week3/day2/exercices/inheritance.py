import math

class Shape:

    def __init__(self, x=0, y=0):
        self.center = (x,y)

    def get_area(self):
        return 0
    
class Rectangle(Shape):
    
    # def __init__(self, x, y):
    #     super().__init__(x,y)

    def get_area(self):
        return (self.center[0] * self.center[1])

class Triangle(Rectangle):

    # def __init__(self, x, y):     # can be left away if the init function is the same.
    #     super().__init__(x,y)

    def get_area(self):
        return super().get_area() / 2

class Circle:

    def __init__(self, radius):
        self.radius = radius

    def get_area(self):
        return math.pi * self.radius * self.radius


class House:

    def __init__(self):
        self.walls = []
        self.holes = []

    def add_wall(self, rect):
        assert isinstance(rect, Rectangle) or isinstance(rect, Triangle)
        self.walls.append(rect)

    def add_hole(self, shape):
        assert isinstance(shape, Rectangle) or isinstance(shape, Triangle) or isinstance(shape, Circle)
        self.holes.append(shape)

    def compute_area_to_paint(self):
        all_walls = 0
        all_holes = 0
        for shape in self.walls:
            all_walls += shape.get_area()
        for shape in self.holes:
            all_holes += shape.get_area()
        return all_walls - all_holes


# print(rect.get_area())
# print(triangle.get_area())
# print(circle.get_area())

house = House()
house.add_wall(Triangle(4, 2))
house.add_wall(Rectangle(4, 3))

house.add_hole(Circle(0.75))
house.add_hole(Rectangle(1, 1))
house.add_hole(Rectangle(2, 1))

print(house.compute_area_to_paint())


