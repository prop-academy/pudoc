/*Create a switch statement that will alert "Hello"
 if fruits is "banana", and "Welcome" if fruits is "apple".*/

var fruits = 'Banana'

switch(fruits) {
    case "Banana":
        alert('Hello');
        break;
    case 'Apple':
      alert('Welcome');
      break;
    default:
      alert('Neither');
}


/*Common Code Blocks

Sometimes you will want different switch cases to use the same code.

In this example case 4 and 5 share the same code block, and 0 and 6 share another code block:
Example
Common Code Blocks

Sometimes you will want different switch cases to use the same code.

In this example case 4 and 5 share the same code block, and 0 and 6 share another code block:*/

// Switch cases use strict comparison (===).
/* When the switch hits the first true, it will run everthing which is below and evaluate to the point it hits "return" or "break" */


switch (new Date().getDay()) {
  case 4:
  case 5:
    text = "Soon it is Weekend";
    break;
  case 0:
  case 6:
    text = "It is Weekend";
    break;
  default:
    text = "Looking forward to the Weekend";
}


let name = 'Laurent'

const role = 'Dean'

