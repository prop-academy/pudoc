# Music Player

Exercise for a JavaScript Music player.

This player should be able to:

- Store a list of tracks
- Add a track
- Play current track
- Move to next track
- Move to previous track

## Solution Approach

The project hosts a GUI made with bootstrap css. What do the different files do?
- `player.js` defines the class `Player` and adds some songs by default. 
- `gui.js` handels the DOM manipulations. The file is loaded inside `index.html` after `player.js`.
- The control buttons are all rendered with javascript using [feather.js](https://feathericons.com/), whereas the `input` and `button` to add a new song are part of `index.html`.


To hook the control methods of the `Player` class to on-click events of the corresponding GUI-icons, the `id` of the control buttons are named the same as the class methods of `Player` (stop, play, next, previous). In this way, a dynamic callback function is attached to the button before rendering. The disadvantage is though, that the table is re-rendered fully on every button click.