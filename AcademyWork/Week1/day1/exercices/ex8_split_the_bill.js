/* Write a function to balance who has overpaid and should be compensated or who has paid less.
The function should take one parameter: a dictionary which represents the members of the group and the amount spent by each.
The function should return an object with the same names, showing how much money the members should pay or receive.
Negative number means they should receive money.
 */


// all of the group should pay the same in other words

const group = {
    Amy: 20,
    Bill: 15,
    Chris: 10
}

function cumSum(arr) {
    function addUp(total, value) { return total + value; }
    return arr.reduce(addUp);
}

function arithmeticMean(arr) {
    return cumSum(arr) / arr.length;
}

function splitTheBill(grouObj) {
    mean = arithmeticMean(Object.values(grouObj));
    let balancedGroup = {};

    for (key in grouObj) {
        balancedGroup[key] = mean - grouObj[key];
    }

    console.log(balancedGroup);
    return balancedGroup;
}


splitTheBill(group); // => { Amy: -5, Bill: 0, Chris: 5 }
