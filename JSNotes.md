## Difference of normal to arrow functions (ES6)

```javascript

function () {
  return this;
}.bind(this);

() => {
  return this;
};

```

The end result seems the same: arrow functions produce a JavaScript function object with their this context bound to the same value as the this where they are created.

How dies the arrow function differ from the function object using a prototype?

1. Arrow functions cannot be used with new. This means, of course, that they do not have a prototype property and cannot be used to create an object with the classically-inspired syntax.  

        new (() => {}) // TypeError: () => {} is not a constructor


2. Arrow functions do not have access to the special arguments object that ordinary JavaScript functions have access to.

        (() => arguments)(1, 2, 3) // ReferenceError: arguments is not defined

    This one is probably a little bit more of a gotcha. Presumably this is to remove one of JavaScript's other oddities. The arguments object is its own special beast, and it has strange behavior, so it's not surprising that it was tossed.

    Instead, ES6 has splats that can accomplish the same thing without any magic hidden variables:

        ((...args) => args)(1, 2, 3) // [1, 2, 3]

3. Arrow functions do not have their own new.target property, they use the new.target of their enclosing function, if it exists.  This is consistent with the other changes to remove "magically" introduced values for arrow functions. This particular change is especially obvious, considering arrow functions can't be used with new anyway, as mentioned above.
