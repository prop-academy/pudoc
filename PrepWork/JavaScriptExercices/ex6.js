// Write a method that takes a string and returns the number of vowels
// in the string. You may assume that all the letters are lower cased. 
// You can treat “y” as a consonant.

function countVowels(string) {

    const vowels = ['a', 'e', 'i', 'o', 'u']
    counter = 0;
    str = string.toLowerCase();

    let i = 0;
    for (i; i < string.length; i++) {

        if (vowels.indexOf(str[i]) !== -1) counter += 1;
    }
    return counter
}

console.log(countVowels('alphabet'))
console.log(countVowels('Propulsion Academy'))
console.log(countVowels('AaaAa')) 
console.log(countVowels('fly'))