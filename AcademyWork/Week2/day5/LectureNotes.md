# SCSS / CSS

- global font size in chrome is 14pt in Google Chrome.
- difference `em` and  `rem`;
- difference between viewhight (`vh`) and percentages: Viewhight is just the visible size of the screen, also when you resize it. With percentages.
- `:root {}` in SASS in normally used for variables or colors or you make a file `_variables.scss` to add them there. With the first solution you don't have to import them in all the files.
- Like the BEM Idea to have a parent `container` and children class `container__main` you would put `&__main {}` inside of `.container{}`.
    ```

    ```