
class myListElement {
    constructor(value) {
        this.value = value;
        this.nextElement = null;
    }
}

class myLinkedList {

    constructor() {
        this.list_head = null;
        this.list_tail = null;
    }

    addToFront(value) {
        const el = new myListElement(value);
        el.nextElement = this.list_head;
        this.list_head = el;
        // point the new element to where the list_head pointed to before
    }

    addToEnd(value) {
        if (this.list_head === null) {
            this.addToFront(value);
        }

        let current = this.list_head;
        while (current.nextElement !== null) {
            current = current.nextElement;
        }

        const newEl = new myListElement(value);
        current.nextElement = newEl;
    }

    removeFromFront() {
        if (this.list_head === null) return;
        this.list_head = this.list_head.nextElement;
    }

    printLast() {
        if (this.list_head === null) {
            console.log('empty');
        }

        let current = this.list_head;
        while (current.nextElement !== null) {
            current = current.nextElement;
        }
        console.log(current);
    }
}

const list = new myLinkedList();

list.addToFront(2);
list.printLast();
list.addToFront(5);
list.printLast();
list.addToEnd(7);
list.printLast();
