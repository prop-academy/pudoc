(function() {
    console.log('hello world');
  })();
  
  /* This “pattern” can be very useful when working in the browser, in order to hide or lock specific values to the end user. */

  const Ball = function() {
    this.vel = [1, 1];
  }
  
  Ball.prototype.move = function() {
    this.pos[0] += this.vel[0];
    this.pos[1] += this.vel[1];
  }
  
  const ball = new Ball();
  
  /* First of all, there is the problem that if you open de console in the DevTools, 
  the user has access to the ball instance. Which means that he could cheat the game. */


  