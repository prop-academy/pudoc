# Porting repos to another remote host

Given the scenario you want to use Gitlab's CI/CD for the same price of a Github Account and port your repo to Gitlab.  
To do this, you have to change the remote host of the repo. Go into your repo folder with `.git` folder in it. **Change to branch master before.**

**ATTENTION**: Your push history and activity gets lost like this. Use the transfer function of Github instead.

```bash

git remote -v
git remote set-url origin git@gitlab.com:username/project_name.git
git remote -v

git push -u origin master

```

Now you can checkout other branches and push them to remote. This will create the branch with the same name on the remote server.

```

git checkout dev
git push origin dev

```