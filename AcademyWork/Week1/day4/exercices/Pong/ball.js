class Ball {
    constructor(x, y, velX, velY, color, size) {
        this.x = x;
        this.y = y;
        this.velX = velX;
        this.velY = velY;
        this.size = size;
        this.color = color;
        this.obstacles = [];
        this.xChDirConds = [];
        this.yChDirConds = [];
    }

    betweenXrange(obj) {
        // console.log('between X Range')
        // console.log(this.x - this.size - obj.x2);
        return ((this.x + this.size) > obj.x1 && (this.x - this.size) < obj.x2)
    }

    betweenYrange(obj) {
        // console.log('between Y Range')8
        return ((this.y + this.size) > obj.y1 && (this.y - this.size) < obj.y2)
    }

    changeXDir(...conditions) {
        if (conditions.indexOf(true) != -1) {
            this.velX = (this.velX) * -1;
            return true
        }
        return false
    }

    changeYDir(...conditions) {
        if (conditions.indexOf(true) != -1) {
            this.velY = (this.velY) * -1;
            debugger
            return true
        }
        return false
    }

    addObstacleObject(obst) {
        this.obstacles.push(obst)
    }

    renderObstacles(color) {
        this.obstacles.forEach(obst => {
            ctx.beginPath()
            ctx.fillStyle = color
            ctx.rect(obst.x1, obst.y1, obst.x2 - obst.x1, obst.y2 - obst.y1);
            ctx.fill();
        })
    }

    hitLeftWall() {
        if (this.x - this.size <= 0) {
            // console.log('Right player scored')
            rightScore += 1
            rightScoreEl.innerHTML = `Points ${rightScore}`
            return true
        }
        return false
    }

    hitRightWall(canvasWidth) {
        if (this.x + this.size >= canvasWidth) {
            // console.log('Left player scored')
            leftScore += 1
            leftScoreEl.innerHTML = `Points ${leftScore}`
            return true
        }
        return false
    }

    move(canvasWidth, canvasHeight) {

        let xChDirC = this.obstacles.map(o => {
            // return this.betweenYrange(o) && this.betweenXrange(o)
            return this.betweenYrange(o) && (this.x === o.x1 || this.x === o.x2)
        })

        let yChDirC = this.obstacles.map(o => {
            // return this.betweenXrange(o) && this.betweenYrange(o)
            return this.betweenXrange(o) && (this.y === o.x1 || this.y === o.y2)
        })


        if (
            this.changeXDir(    // if one of the conditions is true, change Ball's x direction
                // (this.x - this.size) <= 0,
                this.hitRightWall(canvasWidth),
                this.hitLeftWall(),
                ...xChDirC
            )) {
            // console.log('change x-dir')
        } else if (
            this.changeYDir(    // if one of the conditions is true, change Ball's y direction
                (this.y + this.size) >= canvasHeight,
                (this.y - this.size) <= 0,
                ...yChDirC
            )
        ) {
            // debugger
            console.log('change y-dir')
        }

        this.x += this.velX;
        this.y += this.velY;
    }

    render(ctx) {
        ctx.beginPath();
        ctx.fillStyle = this.color;
        ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
        ctx.fill();
    }
}