let a = 1;
const log = function(){console.log(a)}

function doSth() {
    const a = 2;
    const logger = log;
    return logger
}

a = 5;
const logger2 = doSth();
logger2()


/* The function remebers WHERE it was defined. It will try to access a from the top, but at the time of execution, a has changed to 5. */