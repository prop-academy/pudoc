// ECMAScript 2015

/*
ES2015 introduced two important new JavaScript keywords: let and const.

These two keywords provide Block Scope variables (and constants) in JavaScript.

Before ES2015, JavaScript had only two types of scope: Global Scope and Function Scope.
*/

// can be accessed outside of the block, var can not have block scope
{
  var x = 2;
  let y = 3;
}
// y can not be used here

 // Using let in a loop

let i = 5;
for (let i = 0; i < 10; i++) {
 // some statements
}
// Here i is 5
