
class ShoppingList:

    list_items = {}
    cli_info = """Options
    - o: show options
    - s: show items
    - a: add an item
    - r: remove an item
    - q: quit this app.    
    """

    def help_menu(self):

        user_input = None
        print(self.cli_info)

        while user_input != 'q':
            user_input = input('What you want to do next? ')
            if user_input == 'o': self.show_items()
            elif user_input == 's': self.show_items()
            elif user_input == 'a':
                self.add_item(input('What is the name of the new item? '))
            elif user_input == 'r':
                self.delete_item(input('What item id you want to delete? '))
            else: self.show_options()

        print('Bye bye...')

    def show_items(self):
        if len(self.list_items.keys()) == 0: print('No elements in the list')
        for id, name in self.list_items.items():
            print(f'{id}: {name}')

    def add_item(self, name):
        id = str(len(self.list_items.keys()))
        while id in self.list_items.keys():
            id = int(int(id)+1)
        self.list_items[id] = name

    def delete_item(self, id):
        print(f'Deleted item with id {id}: {self.list_items.pop(id)}', )

    def show_options(self):
        print(self.cli_info)


if __name__ == '__main__':
    app = ShoppingList()
    app.help_menu()