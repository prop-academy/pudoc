
function checkAnswer(prompted, questionObj) {
    let corrInd = questionObj.solutionIndex;
    if (typeof prompted === 'string') prompted = Number.parseInt(prompted);
    if (typeof corrInd === 'string') corrInd = Number.parseInt(corrInd);


    if (prompted - 1 === corrInd) {
        alert('Correct!')
        return true
    } else {
        alert(`Wrong answer! The correct answer is ${questionObj.choices[corrInd]}`)
        return true
    }
}

class Question {
    constructor(text, choices, solutionIndex) {
        this.text = text;
        this.choices = choices;
        this.solutionIndex = solutionIndex;
    }

    render(obj) {
        let prefixes = ['1 - ', '2 - ', '3 - ', '4 - '];
        let str = `${obj.text}\n\n`
        obj.choices.forEach((choice, index) => {
            str += `${prefixes[index]}${choice}\n`
        });
        str += '\n'
        return str;
    }

}

class Trivial {

    constructor() {
        this.questions = [];
        this.currentQuestionIndex = null;
    }

    askQuestion(questionObj) {
        let givenAnswer = prompt(questionObj.render(questionObj));
        let continue_game = true;
        // quit the game when somebody hits CANCEL
        console.log(givenAnswer)
        if (! givenAnswer) {
            continue_game = false
            return continue_game
        }
        checkAnswer(givenAnswer, questionObj);
        return continue_game;
    }

    addQuestion(questionObj) {
        this.questions.push(questionObj);
    }

    play() {
        try {
            this.questions.forEach((question, nr) => {
                let continue_game = this.askQuestion(question);
                if (! continue_game) {
                    alert ('Aborting the game....');
                    throw BreakException;
                };
                this.currentQuestionIndex = nr;
            });
        } catch (e) {
            if(e !== BreakException) throw e;
        }
    }
}

question1 = new Question(
    'How much electric cars could you produce with all the Lithium production of the world',
    ['< 100 Mio / y', '< 10 Mio / y', '< 1 Mio / y'], 2)

question2 = new Question(
    'So is the electric car the future for 7000 Million poeple on planet?',
    ['Yes, of course!', 'No, I know there is not enough Lithium.', 'Maybe...?!'], 1)


trivial = new Trivial();
trivial.addQuestion(question1);
trivial.addQuestion(question2);
trivial.play();