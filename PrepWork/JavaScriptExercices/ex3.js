// Write a method that takes in a string. Return the longest word
//  in the string. You may assume that the string contains only letters and spaces. 
// You may use the String split method to aid you in your quest.

let sentence = 'This is an amazing test'

function longest_word(sentence) {
    let longestWord = '';
    sentence.split(' ').forEach(word => {
        if (word.length > longestWord.length) longestWord = word;
    })
    return longestWord
}
console.log(longest_word("This is an amazing test"))
console.log(longest_word("Laurent Colin"))
console.log(longest_word("Propulsion 123"))
