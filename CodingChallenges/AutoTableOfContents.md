# Challenge: Create a script to auto-generate a table of contents of all your work on Gitlab

Any good repository has a `Readme.md` when you visit the githubs or gitlabs landing page.

## What is Markdown?

`*.md` files are recognized by Github/Gitlab etc. as [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).
With Markdown, you express the structure of your document with simple means of what you have on your keybord to write tiny txt files 
that can be rendered to html and read in the browser.

Example? Click on ***View Page Source*** in the top right corner.  

![Some Page rendered with Markdown](./img/example.png)


So all the inline code, lists, tables, links on Momentum - are written in Markdown.

## Presentation on Gitlab

So it would be nice to have a **structured table of contents** of our work that features direct links to the corresponding code.
Since the source of beauty is a simple text file, it can be constructed by a script that crawls through our repository tree (or repositories??!)
and extract and compile this information to a table of contents written in Markdown.  
Example structure (made with `tree` command):

```bash

├── create_table_of_contents.py
├── HowTos
│   ├── Installing_IDEsLinux.md
│   ├── Installing_pyenv_pipenv.md
│   ├── PortRepos.md
│   └── Setting_up_gitlab.md
├── PrepWork
│   ├── Animations
│   │   └── BouncingBalls
│   │       ├── bounce.js
│   │       ├── bouncing_balls.html
│   │       ├── Readme.md
│   │       └── style.css
│   ├── Bank
│   │   ├── bank.js
│   │   └── Readme.md
│   ├── GoogleClone
│   │   ├── google-clone.html
│   │   └── main.css
    |   └── Readme.md

```
Maybe I would rename `ex12.js` to something more meaninful to `Add_all_numbers_in_an_array.js` which makes totally `"sense_for_our_script.".replace("_", " ")`.

## Problem to Solve and the wider picture

The tree structure of your repository could have 4, 5 or more sublevels. How would you desing a small piece of code that can do that?
This is a **typical usecase for a recursive function** that calls itself again based on conditions.
The script will **reduce the dimensionality of data**: We extract all filenames ins the tree and can put them in a 2D table with the corresponding path, that contains information where in the tree it was found.

Below there is an example with Python, that goes trough a json-like object and constructs a path similar to a folder path.

```python

# Some example object that could be a json object as well

ftree = { "name": "repo",
        "children": [
            "Readme.md",
            { "name": "Doc",
            "children": ["testing.md", "installation.md"]},
            {"name": "week1",
            "children": [
                {"name": "day1",
                "children": ["ex1.js", "ex2.js", "ex3.js", "ex4.js", "ex5.js"]},
                {"name": "day2",
                "children": ["ex1.js", "ex2.js", "ex3.js", "ex4.js", "ex5.js",
                     { "name": "BonusExercices", "children": ["bonus1.js", "bonus2.js"]}
                ]}
            ]}
        ]
    }

path = "/"

def print_tree(obj, path):
    name = obj['name']
    children = obj['children']
    path += name + '/'
    for child in children:
        if type(child) is str:
            print(path + child)
        else:
            # path += c['name'] + '/'
            print_tree(child, path)

print_tree(ftree, path)

```  

### Output:

```
/repo/Readme.md
/repo/Doc/testing.md
/repo/Doc/installation.md
/repo/week1/day1/ex1.js
/repo/week1/day1/ex2.js
/repo/week1/day1/ex3.js
/repo/week1/day1/ex4.js
/repo/week1/day1/ex5.js
/repo/week1/day2/ex1.js
/repo/week1/day2/ex2.js
/repo/week1/day2/ex3.js
/repo/week1/day2/ex4.js
/repo/week1/day2/ex5.js
/repo/week1/day2/BonusExercices/bonus1.js
/repo/week1/day2/BonusExercices/bonus2.js

```
