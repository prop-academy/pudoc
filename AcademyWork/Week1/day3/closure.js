/* In this example result was defined inside the service scope, 
which means that it has access to all its scope including token. 
token is impossible to change from outside, and you could never expose it even if you wanted to. 

function service() {
    const token = 'secretToken';
    return {
        printToken: function () {
            console.log('The token is ' + token);
        }
    }
}

const myService = service();
myService.printToken();

Closures make sense if using var, with const and let it's not a problem, it should print 1 to 4 and not only 5

Explanation: var i will in the global scrope. On every loop interation, the function with console.log is pushed to the event
queue. By the time of execution, i will be in the global scope and the loop has already finished. So it goes to get i and will return 5 every time.

*/

function addClosureWrong() {
    return {
        printSecret: function () {
            for (var i = 0; i < 5; i++) {
                setTimeout(() => { console.log(i) }, 100);
            }
        }
    }
}

// with let i, the loop creates its own scope every time and the value i will be preserved.

const closure = addClosureWrong();
closure.printSecret()


function addClosure() {
    return {
        printSecret: function () {
            for (let i = 0; i < 5; i++) {
                setTimeout(() => { console.log(i) }, 100);
            }
        }
    }
}

// Assigning the returned function printSecret to a const.

const closure2 = addClosure();
closure2.printSecret()

// solution with a immediately invoked function with i passed from outside.

for (var i = 0; i < 5; i++) {
    (function (j) {
        setTimeout(() => { console.log('j is', j) }, 100)
    })(i);
}


// with bind that clones the function (copy in memory)
// pass the context null and an additional argument

for (var i = 0; i < 5; i++) {
    setTimeout(function (k) { console.log('k is ', k) }.bind(null, i)
        , 100)

}