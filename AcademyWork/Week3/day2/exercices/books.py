

class Book:

    def __init__(self, title, author):
        self.title = title
        self.author = author

    def __str__(self):
        return f"{self.title} by {self.author}"


class BookShelf:

    def __init__(self, books):
        self.books = [Book(raw_book[0], raw_book[1]) for raw_book in books]

    def __str__(self):
        all_books = ""
        for book in self.books:
            all_books += str(book) + "\n"
        return all_books


book_shelf = BookShelf(books=[
    ('The old man and the see', 'Ernest Hemingway'),
    ('Beyond Good and Evil', 'Friedrich Nietzsche'),
])
print(book_shelf)

