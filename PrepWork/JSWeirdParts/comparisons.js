
undefined === undefined  // true
NaN === NaN     // false, you can not compare two objects

if({} !== {}) console.log('Two empty objects are not the same');
if([] !== []) console.log('Two empty objects are not the same');

console.log('[] + [] = ""', [] + [])
console.log('[] - [] =', [] - [])
console.log('{} + {}', {} + {})
console.log('{} - {}', {} - {})
