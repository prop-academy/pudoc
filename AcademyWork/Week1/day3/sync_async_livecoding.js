// Browser
/* Async functions will run  AFTER all synchronous code. 
Async function are sent to the event loop. 

Async functions:

setTimout
setInterval

*/

let timeout = 0;
let interval = 1000;


console.log('hi')
setTimeout(() => {
    console.log('there 1')
    console.log('there 2')
}, timeout)

setTimeout(() => {
    console.log('there 3 second timeout')
}, timeout)


setInterval(() => { }, interval)

console.log('goodby')
