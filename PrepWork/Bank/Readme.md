# Bank Exercice

Let’s create a Class Bank that will be able to manage the savings of our customers.

The Bank should allow us to:

1. Add customers
2. A customer should be able to deposit an amount.
3. A customer should be able to withdraw an amount.
4. A customer should be able see his/her account.

