// Create a function that returns true when the parameter passed is an array and false otherwise.

function isArray(arr){

    console.log(Array.isArray(arr));
    return Array.isArray(arr);
}

isArray('hello'); // => false
isArray(['hello']); // => true
isArray([2, {}, 10]); // => true
isArray({ a: 2 }); // => fals