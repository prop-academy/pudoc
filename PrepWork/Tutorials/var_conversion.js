//A string can be converted to an array with the split() method:

var txt = "a,b,c,d,e";   // String
txt.split(",");          // Split on commas
txt.split(" ");          // Split on spaces
txt.split("|");          // Split on pipe
txt.split();          // transforms to array with one index 0

//
var txt = "Hello";       // String
txt.split("");           // Split in characters

/*Global JavaScript Methods

JavaScript global methods can be used on all JavaScript data types.

These are the most relevant methods, when working with numbers:
Method 	Description
Number() 	Returns a number, converted from its argument.
parseFloat() 	Parses its argument and returns a floating point number
parseInt() 	Parses its argument and returns an integer
*/
Number(true);          // returns 1
Number(false);         // returns 0
Number("10");          // returns 10
Number("  10");        // returns 10
Number("10  ");        // returns 10
Number(" 10  ");       // returns 10
Number("10.33");       // returns 10.33
Number("10,33");       // returns NaN
Number("10 33");       // returns NaN
Number("John");        // returns NaN

Number(new Date("2017-09-30"));    // returns 1506729600000
// The Number() method above returns the number of milliseconds since 1.1.1970.

//The parseInt() Method

//parseInt() parses a string and returns a whole number. Spaces are allowed. Only the first number is returned:
parseInt("10");         // returns 10
parseInt("10.33");      // returns 10
parseInt("10 20 30");   // returns 10
parseInt("10 years");   // returns 10
parseInt("years 10");   // returns NaN

//The parseFloat() Method

//parseFloat() parses a string and returns a number. Spaces are allowed. Only the first number is returned:
parseFloat("10");        // returns 10
parseFloat("10.33");     // returns 10.33
parseFloat("10 20 30");  // returns 10
parseFloat("10 years");  // returns 10
parseFloat("years 10");  // returns NaN

// JavaScript MIN_VALUE and MAX_VALUE

var x = Number.MAX_VALUE;
var x = Number.MIN_VALUE;
var x = Number.POSITIVE_INFINITY;

//Number properties belongs to the JavaScript's number object wrapper called Number.
//These properties can only be accessed as Number.MAX_VALUE.
var x = 6;
var y = x.MAX_VALUE;    // y becomes undefined