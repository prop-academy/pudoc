
function intersect(first, second) {

    let firstP2 = first[1]
    let secondP1 = second[0]
    if (firstP2[0] < secondP1[0]) return null;  // rectangle has no x intersection, no surface
    if (firstP2[1] < secondP1[1]) return null;  // rectangle has no y intersection, no surface

    // define the left rect as left, the right one as right
    let left = second;
    let right = first;
    if (first[0][0] < second[0][0]) {
        left = first;
        right = second;
    }

    let leXmin = left[0][0];
    let leYmin = left[0][1];
    let leXmax = left[1][0];
    let leYmax = left[1][1];
    let riXmin = right[0][0];
    let riYmin = right[0][1];
    let riXmax = right[1][0];
    let riYmax = right[1][1];


    let newXmin = leXmin > riXmin ? leXmin : riXmin;
    let newXmax = leXmax < riXmax ? leXmax : riXmax;

    let newYmin = leYmin > riYmin ? leYmin : riYmin;
    let newYmax = leYmax < riXmax ? leYmax : riYmax;

    console.log([newXmin, newYmin], [newXmax, newYmax])
    return [newXmin, newYmin], [newXmax, newYmax]
}


intersect([[1, 1], [4, 3]], [[2, 2], [6, 7]]); // => [2, 2], [4, 3]
intersect([[2, 2], [6, 7]], [[1, 1], [4, 3]]); // => [2, 2], [4, 3]
intersect([[2, 1], [4, 4]], [[1, 1], [8, 8]]); // => [2, 1], [4, 4]
intersect([[1, 1], [3, 3]], [[4, 4], [7, 7]]); // null
