
class Player {
    constructor() {
        this.playlist = [];
        this.trackId = 0;
        this.is_playing = false;
    }

    add(title) {
        this.playlist.push(title);
    }

    addtoTop(title) {
        this.playlist.unshift(title);
    }

    play() {
        if (!this.playlist.length === 0) return console.log('No tracks in the playlist');
        this.is_playing = true;
        return console.log(`Playing track:  "${this.playlist[this.trackId]}"`);
    }

    stop() {
        this.is_playing = false;
        return console.log("Stopping...");
    }

    next() {
        if (!(this.trackId == this.playlist.length - 1)) this.trackId += 1;
        this.play();
    }

    previous() {
        if (!(this.trackId == 0)) this.trackId -= 1;
        this.play();
    }

    displayTracklist() {
        console.log('Tracks');
        let pl = this.playlist;
        console.log(pl.length)
        pl.forEach(element => {
            console.log('-' + element);
        });
    }
}


// CHECKS

player = new Player();

player.add('The greatest song in the world');
player.add('Black Bird');
player.add('The Bard Song');
player.addtoTop('My alltime favorite song');
// player.displayTracklist();
// player.play();
// player.next();
// player.next();
// player.next();
// player.next();
// player.next();
// player.previous();

// player.trackId = 0;

// player.previous();

