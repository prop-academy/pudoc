var trackDiv = document.getElementById('trackdiv');
var controlDiv = document.getElementById('controls');
var inputSong = document.getElementById('input-song');
var addButton = document.getElementById('add');


// create a th element to append in thead as child
function newTh (header){   
    let th = document.createElement('th');
    th.textContent = header;
    return th;    
}
// create tr element to append to tbody
function newTr(id, className){
    let tr = document.createElement('tr');
    tr.className = className;
    tr.id = id;
    return tr;
}

function newTd(content) {
    let td = document.createElement('td');
    td.className = 'p-l-2';
    td.innerText = content;
    return td;
}

function newFeatherIconButton(id, btnClass, iconName){
    cl = btnClass || 'btn';

    let btn = document.createElement('button');
    btn.className = cl;
    btn.id = id;

    let i= document.createElement('i');
    // Render the icons later with feather.replace()
    i.setAttribute('data-feather', iconName);
    
    btn.appendChild(i);
    return btn;
}


function createTrackTable(playlist, active_ind, is_playing){
    // for now we have just song title in the array

    // Create a whole table
    let newTable = document.createElement('table');
    newTable.className = 'table table-sm';
    let thead = document.createElement('thead');
    let tbody = document.createElement('tbody');

    // console.log('create table, playing', is_playing);

    let header = 'Song Title';
    let th = newTh(header);
    thead.appendChild(th);
        

    for(let i in playlist){

        let classNameTr = '';
        if(i == active_ind){
            classNameTr = 'table-active ';
            if(is_playing) classNameTr = 'table-success font-weight-bold';
        }

        let tr = newTr(id=i, className=classNameTr);     
        let song = playlist[i];        
        let td = newTd(song);

        tr.appendChild(td);
        tbody.appendChild(tr);
    };

    newTable.appendChild(thead);
    newTable.appendChild(tbody);

    // Remove exiisting
    while (trackDiv.firstChild) {
        trackDiv.removeChild(trackDiv.firstChild);
    }
    // Add the new table
    trackDiv.appendChild(newTable);

}

addButton.onclick = function(){
    let title = inputSong.value;
    console.log('Add new song: ', title);
    player.add(title);
    createTrackTable(player.playlist, player.trackId, player.is_playing);
};


// Hooks for callbacks for controls

function createControls(width=40, height=40){
    // id is prefixed here with feather
    let ficons = {
    ficon_play : 'play-circle',
    ficon_stop : 'stop-circle',    
    ficon_previous : 'arrow-left-circle',
    ficon_next : 'arrow-right-circle'
    };        

    for(let key in ficons){
        // start, stop, next, previous
        let id = key.split('_')[1];
        let btn = newFeatherIconButton(
            id, 'btn', ficons[key]         
        );

        // Create controls directly in here, using logic
        console.log(id);
        console.log(player[id]);
        let callback = function(){
            player[id]();
            createTrackTable(player.playlist, player.trackId, player.is_playing);
        };
        btn.onclick = callback;
        controlDiv.appendChild(btn);
    };

    feather.replace({width: width, height: height});    
}

// Renders the controls as icons and calls feather.replace() to render icons
createControls();
// Populates the tracklist
createTrackTable(player.playlist, player.trackId, player.is_playing);

