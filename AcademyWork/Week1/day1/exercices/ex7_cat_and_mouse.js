/* You will be given a string featuring a cat ‘C’ and a mouse ‘m’. The rest of the string will be made up of ‘.’.

You need to find out if the cat can catch the mouse from it’s current position. The cat can jump three characters. */


let catJumpPower = 3;

function isCaught(str){
    let cat = 'C';
    let mouse = 'm';

    let catPos = str.indexOf(cat);
    let mousePos = str.indexOf(mouse);

    if(mousePos - catPos <= catJumpPower) {
        console.log('true');
        return true;    
    }
    console.log('false');
    return false;
}

isCaught('C.....m') // => false
isCaught('C..m') // => true
isCaught('..C..m') // => true
