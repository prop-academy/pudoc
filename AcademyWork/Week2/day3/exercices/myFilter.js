// callback function must be returning boolean
function myFilter(arr, callback) {
    let r = [];
    for(let i=0; i < arr.length; i++){
        if(callback(arr[i], i)=== true) r.push(arr[i]);
    }
    return r;
}

let myList = [1,2,3,5];

// myMap(myList, el => {return el * 2})
const filterFcts = el => el < 3;
console.log(myFilter(myList, filterFcts))