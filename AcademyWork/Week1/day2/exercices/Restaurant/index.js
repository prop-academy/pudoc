function sumArr(arr) {
    return arr.reduce((total, additional) => total + additional)
}

function printLine() { console.log('_____________________________________') }


class Ingredient {
    constructor(name, cost) {
        this.name = name;
        this.cost = cost;
    }
}

const cheese = new Ingredient('Cheese', 5);
const pepperoni = new Ingredient('Pepperoni', 1);
const dough = new Ingredient('Dough', 0.5);
const lettuce = new Ingredient('Lettuce', 1);
const tomato = new Ingredient('Tomato', 1.5);

class Dish {
    constructor(name, ingredients, price) {
        this.name = name;
        this.ingredients = ingredients;
        this.fixCost = 10;
        this.price = price;
    }

    cost() {
        let allCosts = this.ingredients.map(ing => {
            return ing.cost;
        })

        // let cost = allCosts.reduce((total, additional) => { return total + additional}, 0)
        let cost = sumArr(allCosts)
        return cost + this.fixCost;
    }

    profit() {
        return this.price - this.cost();
    }
}

const pizza = new Dish('Pizza', [cheese, pepperoni, dough], 24)
const salad = new Dish('Salad', [lettuce, cheese, tomato], 21)

class Restaurant {
    constructor() {
        this.orders = [];
    }

    orderDish(dishObj, client) {
        let order = {};
        order.id = client.id
        order.dish = dishObj;
        this.orders.push(order);
    }

    // check() {
    //     let totalCost = [];
    //     this.orders.forEach((dish, index) => {
    //         totalCost.push(dish.cost())
    //         console.log(`Order #${index}: ${dish.name} - ${dish.cost()}`
    //         )
    //     });
    //     console.log(`Total: ${sumArr(totalCost)}`)
    // }

    getClientOrders(client) {
        return this.orders.filter(order => order.id === client.id)
    }

    printCheck(client) {
        let clientOrders = this.getClientOrders(client)

        //     let clientOrder = this.orders[client.id];
        let totalCost = [];

        printLine()
        console.log(`${client.name}`);
        clientOrders.forEach((order, index) => {
            totalCost.push(order.dish.cost())
            console.log(`Order #${index}: ${order.dish.name} - ${order.dish.cost()}`)
        })
        printLine();
    }

    getProfitFromOrderList(orders) {
        let allProfits = [];
        orders.forEach(order => {
            allProfits.push(order.dish.profit())
        })
        let totalProfit = sumArr(allProfits);
        return totalProfit;
    }

    totalProfit() {
        let result = this.getProfitFromOrderList(this.orders);
        console.log(`total profit of restaurant is ${result}`);
        return result;
    }

    profitbyClient(client) {
        let orders = this.getClientOrders(client);
        let result = this.getProfitFromOrderList(orders);
        console.log(`total profit of client ${client.name} is ${result}`);

        return result;

    }

}

class Client {
    constructor(name, id) {
        this.name = name;
        this.id = id;
    }
}


pluto = new Client('Pluto', 1);
goofy = new Client('Goofy', 2);

restaurant = new Restaurant();

// restaurant.orderDish(pizza);
// restaurant.orderDish(salad);
// restaurant.check();

restaurant.orderDish(pizza, goofy);
restaurant.printCheck(goofy);
restaurant.orderDish(pizza, pluto);
restaurant.orderDish(salad, pluto);
restaurant.printCheck(pluto);

// Bonus work

restaurant.totalProfit();
restaurant.profitbyClient(goofy);






