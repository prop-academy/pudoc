'use strict';

class Block {
    constructor(ctx, width, height, position, color) {
        this.ctx = ctx;
        this.width = width;
        this.height = height;
        this.position = position;
        this.color = color;
        this.visible = true;
    }

    render() {
        if(!this.visible) return;
        this.ctx.beginPath()
        this.ctx.fillStyle = this.color
        this.ctx.strokeStyle = "rbga(0,0,0,0.8)";
        this.ctx.lineWidth = 1;
        this.ctx.rect(this.position[0], this.position[1], this.width, this.height);
        this.ctx.stroke();
        this.ctx.fill();
    }
}
