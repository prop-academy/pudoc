## GOAL: Scan through folder AcademyWork and auto-create a Markdown File that lists the contents

ftree = { "name": "repo",
        "children": [
            "Readme.md",
            { "name": "Doc",
            "children": ["testing.md", "installation.md"]},
            {"name": "week1",
            "children": [
                {"name": "day1",
                "children": ["ex1.js", "ex2.js", "ex3.js", "ex4.js", "ex5.js"]},
                {"name": "day2",
                "children": ["ex1.js", "ex2.js", "ex3.js", "ex4.js", "ex5.js",
                     { "name": "BonusExercices", "children": ["bonus1.js", "bonus2.js"]}
                ]}
            ]}
        ]
    }

path = "/"

def print_tree(obj, path):
    name = obj['name']
    children = obj['children']
    path += name + '/'
    for child in children:
        if type(child) is str:
            print(path + child)
        else:
            # path += c['name'] + '/'
            print_tree(child, path)

print_tree(ftree, path)