// All numbers in JavaScript are stored as 64bit floating point numbers

// Precision
// Integers (numbers without a period or exponent notation) are accurate up to 15 digits.

var x = 999999999999999;   // x will be 999999999999999
var y = 9999999999999999;  // y will be 10000000000000000

// The maximum number of decimals is 17, but floating point arithmetic is not always 100% accurate:
var x = 0.2 + 0.1;         // x will be 0.30000000000000004
// To solve the problem above, it helps to multiply and divide:
var x = (0.2 * 10 + 0.1 * 10) / 10;       // x will be 0.3

// NaN
var x = 100 / "Apple";  // x will be NaN (Not a Number)
// global javascript function to check if x is nan
isNaN(x);               // returns true because x is Not a Number

var x = 100 / "10";     // x will be 10

// Watch out for NaN. If you use NaN in a mathematical operation, the result will also be NaN:
var x = NaN;
var y = 5;
var z = x + y;         // z will be NaN

var x = NaN;
var y = "5";
var z = x + y;         // z will be NaN5

typeof NaN;            // returns "number"

//Infinity

// is the value JavaScript will return if you calculate a number outside the largest possible number.
var myNumber = 2;
while (myNumber != Infinity) {          // Execute until Infinity
  myNumber = myNumber * myNumber;
}

// Division by 0 (zero) also generates Infinity:
var x =  2 / 0;          // x will be Infinity
var y = -2 / 0;          // y will be -Infinity
typeof Infinity;        // returns "number"

//Hexadecimal

// JavaScript interprets numeric constants as hexadecimal if they are preceded by 0x.
var x = 0xFF;           // x will be 255

// Number Objects
var x = 500;
var y = new Number(500);

// (x == y) is true because x and y have equal values
// (x === y) is false because x and y do not have equal type

var x = new Number(500);
var y = new Number(500);

// (x == y) is false because objects cannot be compared

// toExponential method
var x = 9.656;
x.toExponential(2);     // returns 9.66e+0
x.toExponential(4);     // returns 9.6560e+0
x.toExponential(6);     // returns 9.656000e+0
// The parameter is optional. If you don't specify it, JavaScript will not round the number.

//The toFixed() Method

//toFixed() returns a string, with the number written with a specified number of decimals:
var x = 9.656;
x.toFixed(0);           // returns 10
x.toFixed(2);           // returns 9.66
x.toFixed(4);           // returns 9.6560
x.toFixed(6);           // returns 9.656000

//The toPrecision() Method

//toPrecision() returns a string, with a number written with a specified length:
var x = 9.656;
x.toPrecision();        // returns 9.656
x.toPrecision(2);       // returns 9.7
x.toPrecision(4);       // returns 9.656
x.toPrecision(6);       // returns 9.65600

//The valueOf() Method

// valueOf() returns a number as a number.
// The valueOf() method is used internally in JavaScript to convert Number objects to primitive values.
var x = 123;
x.valueOf();            // returns 123 from variable x
(123).valueOf();        // returns 123 from literal 123
(100 + 23).valueOf();   // returns 123 from expression 100 + 23

// Sorting numerically
/*
By default, the sort() function sorts values as strings.
This works well for strings ("Apple" comes before "Banana").
However, if numbers are sorted as strings, "25" is bigger than "100", because "2" is bigger than "1".
Because of this, the sort() method will produce incorrect result when sorting numbers.
You can fix this by providing a compare function:
*/
var points = [40, 100, 1, 5, 25, 10];
points.sort(function(a, b){return a - b});
// Use the same trick to sort an array descending:
points.sort(function(a, b){return b - a});

// Sorting in random order
var points = [40, 100, 1, 5, 25, 10];
points.sort(function(a, b){return 0.5 - Math.random()});

// Highest and lowest value
function myArrayMax(arr) {
  return Math.max.apply(null, arr);
}
// Math.max.apply([1, 2, 3]) is equivalent to Math.max(1, 2, 3).
function myArrayMin(arr) {
  return Math.min.apply(null, arr);
}