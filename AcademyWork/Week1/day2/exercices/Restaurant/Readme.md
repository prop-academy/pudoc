# Restaurant Exercice

This is a class-based exercice written in ES6. 

- Create Dishes made from igredients that have a price and a cost and profit per Dish
- Add dished to the restaurant orders list as a Client
- print the clients orders to the console
- calculate total and per client profit (BONUS)
