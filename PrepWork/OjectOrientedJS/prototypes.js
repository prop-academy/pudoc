/* prototype instead is a property containing an object on which you define members that you want to be inherited.
A prototype is another object that is used as a fallback source of properties. 
Get the prototype of the "Object" object */

console.log(Object.getPrototypeOf(Object))      // function


function Car(brand, color, price){
    this.brand = brand;
    this.color = color;
    this.price = price;

    this.info = function (){
        return `This car is a ${this.color} ${this.brand} and costs ${this.price} canadian rupees.`
    };
}

let car1 = new Car('Lotus', 'green', 56000);
console.log(c.info());


// __proto__ gets inherited, other object methods aren't
Object.getPrototypeOf(car1);      // Car {}
Car.prototype;                // Car {}
car1.prototype;                    // undefined, it is an instance of Car, but car1.constructor.prototype yields Car {}
car1.__proto__;                   // the same, but older syntax
car1.valueOf();                   // Car { brand: 'Lotus', color: 'green', price: 56000, info: [Function] }

Object.getPrototypeOf(Car);    // [Function]
Car.valueOf();  // [Function: Car]


// Create a new object based on the prototype of car1

let car2 = Object.create(car1)  // copies attributes and values
car2.__proto__      // will be the car1 object

car1.constructor == car2.constructor            // true
car1.constructor.prototype == Car.prototype     // true
