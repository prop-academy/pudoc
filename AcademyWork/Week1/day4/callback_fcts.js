function doSthg() {
    let a = 2;
    // const log = () => console.log(a)
    const log = function(){console.log(a)}
    return log;
}

a = 100;

const logger = doSthg()

logger()   // will output 2, it remembers where the function was defined, changing a to var does not change things