/* Compare two arrays and return a new array with any items only found in one of the two given arrays,
 but not both. In other words, return the symmetric difference of the two arrays.
 */


/* 

Assumption: No duplicates in arrays itself.

Solution 1:

- extract unique in both arrays
- concat array
- delete all that occur twice and return

Solution 2:

- take array one, iterate over elements and try to find it in array2
- if not found, push to resulting list
- repeat for the second array

Solution 3 (probabely best):
- iterate over every element in first array and try finding it in second
- if prensent, delete element in both arrays
- concat arrays and return

*/

// why is array[i+1] undefined????

function diffArray(arr1, arr2) {

    let result = [];
    let both = arr1.concat(arr2);
    // console.log(both);
    for (let el of both) {
        console.log(el)
        if (arr1.indexOf(el) === -1 || arr2.indexOf(el) === -1) {
            result.push(el);
        }
    }
    console.log(result);
    return result;
}

const diffArrayES6 = (arr1, arr2) => {
    return [...arr1, ...arr2].filter(el => (arr1.indexOf(el) === -1 || arr2.indexOf(el) === -1))
}

diffArray(
    ["andesite", "grass", "dirt", "pink wool", "dead shrub"],
    ["diorite", "andesite", "grass", "dirt", "dead shrub"]
) // [ 'pink wool', 'diorite' ]
