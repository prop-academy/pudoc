// 

class myListElement {
    constructor(value) {
        this.value = value;
        this.nextElement = null;
        this.prevElement = null;
    }
}

class Queue {

    constructor() {
        this.listHead = null;
        this.listTail = null;
    }

    enqueue(value) {
        const el = new myListElement(value);
        // make the new element the previous of the now second element
        if (this.listHead !== null) {
            this.listHead.prevElement = el
        } else {
            // make this element the last one if it is registered as the newest one.
            this.listTail = el;
        };
        // set the next element of the new element to be 
        el.nextElement = this.listHead
        this.listHead = el;
    }

    dequeue() {
        // remove from end and return element, mark second to last el as last el.
        if (this.listHead === null) return null;

        let r = this.listTail
        let prevEl = this.listTail.prevElement;
        prevEl.nextElement = null;
        this.listTail = prevEl;
        return r;
    }

    // Returns front-most element from queue, but doesn't remove it
    newest() {
        return this.listHead;
    }

    last() {
        return this.listTail;
    }

    // Return true if queue has no elements in it, otherwise false
    isEmpty() {
        return this.listHead === null;
    }

    // Remove all elements from queue
    clear() {
        this.listHead = null;
        this.listTail = null;
    }
}

qu = new Queue();
console.log(qu.isEmpty());  // true
qu.enqueue(12);
console.log(qu.isEmpty());  // false
console.log(qu.newest().value)     // 12
qu.enqueue(24);
console.log(qu.newest().value)     // 24
qu.enqueue(48);
console.log(qu.last().value)     // 12
let dequeued = qu.dequeue();
console.log(dequeued.value);    // 12 
console.log(qu.newest().value)     // 48


// console.log()
// console.log()
// console.log()
// console.log()
// console.log()