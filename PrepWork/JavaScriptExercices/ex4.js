// Write a method that takes in an integer num and returns the sum of all integers
// between zero and num, up to and including num.

// Will be quicker as a for loop

function sumNumsGauss(num) {
    if (num === 0) return 0;
    // Check if number is even
    if (num % 2 === 0) {
        return (num + 1) * num / 2;
    } else {
        return (num + 1) * Math.floor(num / 2) + Math.ceil(num / 2);
    }
}

function sumNums(num) {
    if (num === 0) return 0;
    let i = num;
    let result = 0;
    for (i; i > 0; i--) {
        result += i;
    }
    return result
}

console.log('Solution with for Loop')
console.log(sumNums(6))
console.log(sumNums(1))
console.log(sumNums(0))


console.log('Timing two solutions with 1e8')
console.time('sumNums');
sumNums(1e8);
console.timeEnd('sumNums');


console.time('sumNumsGauss');
sumNumsGauss(1e8);
console.timeEnd('sumNumsGauss');