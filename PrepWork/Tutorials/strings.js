// Caracter escaping
/*
\b 	Backspace
\f 	Form Feed
\n 	New Line
\r 	Carriage Return
\t 	Horizontal Tabulator
\v 	Vertical Tabulator
*/
var x = "The character \\ is called backslash.";
var x = 'It\'s alright.';
var x = "We are the so-called \"Vikings\" from the north.";

// You can also break up a code line within a text string with a single backslash:
// But avoid it, since some browsers complain with spaces behind \

// Properties

x.length;

// Finding a string in a string, counting from 0
var str = "Please locate where 'locate' occurs!";
var startPos = str.indexOf("locate");   // 7
// The lastIndexOf() method returns the index of the last occurrence of a specified text in a string:
var endPos = str.lastIndexOf("locate");     // 21

// nothing found returns -1
str.lastIndexOf("John");
// both accept starting position for the search
var pos = str.indexOf("locate",15);

// search method, can use regex,too
str.search('locate')

// Accessing characters

var str = "HELLO WORLD";
str.charAt(0);            // returns H

// returns a utf-16 code (an integer between 0 and 65535).
var str = "HELLO WORLD";
str.charCodeAt(0);         // returns 72

// ECMAScript 5 (2009) allows property access [ ] on strings:
str[0];                   // returns H

/*
Property access might be a little unpredictable:

    It does not work in Internet Explorer 7 or earlier
    It makes strings look like arrays (but they are not)
    If no character is found, [ ] returns undefined, while charAt() returns an empty string.
    It is read only. str[0] = "A" gives no error (but does not work!)

*/
var str = "HELLO WORLD";
str[0] = "A";             // Gives no error, but does not work
str[0];                   // returns H

// Slicing

//Extracting String Parts

//There are 3 methods for extracting a part of a string:

//    slice(start, end)
//    substring(start, end)
//    substr(start, length)

str.slice(7,21);    // from 7 to 21
str.slice(21);  // from 21 to the end

// The difference is that substring() cannot accept negative indexes.

// String replacement

// replaces the first valid match, case sensitive
str = "Please visit Microsoft!";
var n = str.replace("Microsoft", "W3Schools");

// make it case insensitive with regex
str = "Please visit Microsoft!";
var n = str.replace(/MICROSOFT/i, "W3Schools");

// replace all matches with /g (global match)
str = "Please visit Microsoft and Microsoft!";
var n = str.replace(/Microsoft/g, "W3Schools");

// Lower and uppercase
var text1 = "Hello World!";       // String
var text2 = text1.toUpperCase();  // text2 is text1 converted to upper
var text3 = text1.toLowerCase();

// concat joins one ore more strings
var text1 = "Hello";
var text2 = "World";
var text3 = text1.concat(" ", text2);

// String.trim() removes whitespace from both sides of a string.
var str = "       Hello World!        ";
alert(str.trim());

// Joining

text3.join(",");

// Sorting

// the sort function can be used for string
var fruits = ['Banana', 'Cherry', 'Apple', 'Mango'];
fruits.sort();

