/* The Bank should allow us to:

1. Add customers
2. A customer should be able to deposit an amount.
3. A customer should be able to withdraw an amount.
4. A customer should be able see his/her account.

written in ES5

*/

function Bank() {
    this.customers = {};
}

Bank.prototype.addCustomer = function (name) {
    // this.customers[name] always points to undefined
    this.customers[name] = 0;
};

Bank.prototype.printAccount = function (name) {
    console.log(name + "\'s accounts is " + this.customers[name]);
}

Bank.prototype.allAccounts = function () {
    for (let name in this.customers) {
        console.log(name + ':', this.customers[name]);
    }
}

Bank.prototype.deposit = function (name, amount) {
    this.customers[name] += amount;
}

Bank.prototype.withdraw = function (name, amount) {
    let savings = this.customers[name];
    if ((savings - amount) > 0) {
        // this.customers[name] -= amount;
        savings -= amount;
    } else {
        console.log('You have only ' + savings + ' left in your account.');
    };
}


var bank = new Bank();
bank.addCustomer('john');
bank.printAccount('john');
bank.deposit('john', 5);
bank.printAccount('john');
bank.withdraw('john', 3);
bank.printAccount('john');

// CHECKS

bank.addCustomer('Sheldon');
bank.printAccount('Sheldon');
bank.deposit('Sheldon', 10);
bank.printAccount('Sheldon');
bank.addCustomer('Raj');
bank.printAccount('Raj');
bank.deposit('Raj', 10000);
bank.printAccount('Raj');
bank.withdraw('Raj', 100);
bank.printAccount('Sheldon'); // this should print 'Sheldon account is 10'
bank.printAccount('Raj'); // this should print 'Raj account is 9900'

// Additions

bank.allAccounts();
bank.withdraw('john', 100);