# Javasript and data structure

## Exmample: Array

### Create a new array

We create a new list `let a = [1];`

- a block where it stores the length (and array capacity) of the list at block 100.
- in a block further down the first element at block 101.

### Push an element to the list

- `a.push(2)` writes the whole array again somewhere else to add another element `O(n)`.
- an insert into the array is also `O(n)` and it can not be optized, since all the following elements have to be moved by 1 block.
- `push` in javascript increases capacity by 2x on each insert. If I insert 10 elements, the array will be expanded 20 elements, so that the next 10 elements can be
written without a cost. But you have `O(n)` "unused" memory.

## [Link Lists](./exercices/LinkedList.js)

Every block has a link to where the next element is stored. This is the next-pointer. Accessing an arbitrary element needs `O(n)` where in the list it is quicker `O(1)`.
There might also be a single tail pointer to know the last element. Deleting the last element is costly, since the second to last will need to have the last-element pointer. Since the last element is not know, it starts from the beginning to look for it `O(n)`.

- Adding an element to the end or beginning of the list is quick.

### [Double Link List](./exercices/DoubleLinkedList.js)

The double link list that has a next-pointer AND and a previous pointer. This solves some of the previous problems of the linked list.  


### [Binary tree](./exercices/BinaryTree.js)

- used for sorted data
- adding a new data 
- depth of the tree is `log2(n)`
- everything is `log2(n)` except for a degenerated tree (numbers already sorted).
