
class Ingredient:

    def __init__(self, name, cost, price):
        self.name = name
        self.cost = cost
        self.price = price


cheese = Ingredient('Cheese', 4, 6)
peperoni = Ingredient('Peperoni', 1, 3)
dough = Ingredient('Dough', 4, 5)
lettuce = Ingredient('Lettuce', 1, 2)
tomato = Ingredient('Tomato', 1, 2)


class Dish:

    def __init__(self, name, ingredients):
        self.name = name
        self.ingredients = ingredients

    def cost(self):
        total_cost = 0
        for ingr in self.ingredients:
            total_cost += ingr.cost
        return total_cost

    def price(self):
        total_price = 0
        for ingr in self.ingredients:
            total_price += ingr.price
        return total_price

    def profit(self):
        return self.price() - self.cost()

pizza = Dish('Pizza', [cheese, peperoni, dough])
salad = Dish('Salad', [lettuce, cheese, tomato])

class Customer:
    def __init__(self, name):
        self.name = name


goofy = Customer('Goofy')
pluto = Customer('Pluto')

class Restaurant:

    def __init__(self):
        self.orders = []

    def order_dish(self, dish, customer):
        order = dict()
        order['customer'] = customer.name
        order['dish'] = dish
        self.orders.append(order)

    def print_check_by_customer(self, customer_name):
        customer_orders = [order for order in self.orders if order['customer'] == customer_name]
        if not customer_orders:
            return f"Customer {customer_name} has no orders"

        order_list = f"Customer: {customer_name}\n"

        cost_total = 0
        for i, order in enumerate(customer_orders):
            cost = float(order['dish'].cost())
            cost_total += cost
            name = order['customer']
            order_list += f" - Order #{i} {name} - {cost}.-\n"

        order_list += f'Total: {cost_total}.-'
        print(order_list)
        # return order_list

    def print_checks(self):
        customers_in_line = [order['customer'] for order in self.orders]
        for customer in customers_in_line:
            self.print_check_by_customer(customer)

    def total_profit(self):
        total_profit = 0
        for order in self.orders:
            total_profit += order['dish'].profit()
        print('Total Profit: ', total_profit)
        return total_profit

"""
Why I did not use a dictionary to store the order of the restaurant: The dict is not ordered, so better use a list
 of objects that simulates a queue.
"""


restaurant = Restaurant()
restaurant.order_dish(pizza, goofy)
restaurant.order_dish(pizza, goofy)
restaurant.order_dish(salad, pluto)
restaurant.order_dish(salad, pluto)
restaurant.order_dish(salad, pluto)
# restaurant.print_check_by_customer('Goofy')
restaurant.print_checks()
restaurant.total_profit()
# print(pizza.cost())

