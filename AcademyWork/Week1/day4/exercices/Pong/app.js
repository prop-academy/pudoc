// Create random number in the range of two numbers

var canvas = document.getElementById('game-canvas');
var leftScoreEl = document.getElementById('left');
var rightScoreEl = document.getElementById('right');
var leftScore = 0;
var rightScore = 0;
var ctx = canvas.getContext('2d')

function renderCanvas() {
    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}
// What happens with this command? 
// ctx.scale(10, 10);

// Just to debug in the browser, remove for game later

class Game {
    constructor() {

        // GAME PARAMETERS

        this.randomDirection(10);
        this.backgroundColor = 'black';
        this.paddleHeight = 70;
        this.paddleVelocity = 30;
        this.upKeyLeftPlayer = 'w'
        this.downKeyLeftPlayer = 's'
        this.upKeyRightPlayer = 'ArrowUp'
        this.downKeyRightPlayer = 'ArrowDown'
    }

    randomDirection(speed) {
        let max = 1;
        let min = -1;
        let x = Math.floor(Math.random() * (max - min + 1)) + min;
        let y = Math.floor(Math.random() * (max - min + 1)) + min;
        let magnitude = Math.sqrt(x * x + y * y);
        this.ballVelX = speed * x / magnitude;
        this.ballVelY = speed * y / magnitude;
    }

    play() {

        let paddle1 = new Paddle(10, canvas, this.upKeyLeftPlayer, this.downKeyLeftPlayer, this.paddleVelocity);
        let paddle2 = new Paddle(canvas.width - 15, canvas, this.upKeyRightPlayer, this.downKeyRightPlayer, this.paddleVelocity);

        let ball = new Ball(canvas.width / 2, canvas.height / 2, this.ballVelX, this.ballVelY, 'red', 5);

        let testObstacle = { x1: 100, x2: 600, y1: 360, y2: 400 };
        ball.addObstacleObject(paddle1)
        ball.addObstacleObject(paddle2)
        ball.addObstacleObject(testObstacle)

        document.addEventListener('keydown', e => {
            if (e.key === ' ') {
                console.log(e);                
            }
        })

        setInterval(() => {
            renderCanvas(ctx);
            paddle1.render(ctx);
            paddle2.render(ctx);
            ball.render(ctx);
            ball.renderObstacles('white');
            ball.move(canvas.width, canvas.height);
        }, 17);


        // window.requestAnimationFrame(play);
    }
}
var game = new Game();
game.play();

// function PackMan(x, y, radius, ctx) {

//     ctx.beginPath();
//     ctx.arc(x, y, radius, 1 / 4 * Math.PI, 5 / 4 * Math.PI, false);
//     ctx.fillStyle = 'yellow';
//     ctx.fill();

//     ctx.beginPath();
//     ctx.arc(x, y, radius, 3 / 4 * Math.PI, 7 / 4 * Math.PI, false);
//     ctx.fillStyle = 'yellow';
//     ctx.fill();
// }

// PackMan(100,100, 30)