String.prototype.reverse = function(str){
    // return str.split("").reverse().join('');
    // to get the current string of the String instance, this.valueOf() works
     return this.valueOf().split("").reverse().join('');
}

let myString = 'babel';
console.log(myString.reverse());