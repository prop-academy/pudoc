// Create a function that returns true when the argument passed is a string and false otherwise.

function isString(s) {
    if (typeof s === 'string') {
        console.log('true');
        return true;
    }
    console.log('false');
    return false;
}

isString('hello'); // => true
isString(['hello']); // => false
isString('this is a long sentence'); // => true
isString({ a: 2 }); // => false