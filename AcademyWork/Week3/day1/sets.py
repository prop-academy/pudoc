empty_set = set()
set_from_list = set([1, 2, 1, 4, 3]) # => {1, 3, 4, 2}

basket = {"apple", "orange", "apple", "pear", "banana"}
len(basket) # => 4
"orange" in basket # => True
"crabgrass" in basket # => False

for fruit in basket:
    print(fruit, end='/')
# => pear/banana/apple/orange/

# Common set operations
a = set("mississippi") # {'m', 'i', 'p', 's'}

a.add('r') # {'p', 's', 'm', 'r', 'i'}

a.remove('m') # raises KeyError if 'm' is not present
a.discard('x') # same as remove, except no error

a.pop() # => 's' (or 'i' or 'p')
a.clear()

len(a) # => 0

a = set("abracadabra") # {'a', 'r', 'b', 'c', 'd'}
b = set("alacazam") # {'a', 'm', 'c', 'l', 'z'}

# Set difference
a - b # => {'r', 'd', 'b'}

# Union
a | b # => {'a', 'c', 'r', 'd', 'b', 'm', 'z', 'l'}

# Intersection
a & b # => {'a', 'c'}

# Symmetric Difference
a ^ b # => {'r', 'd', 'b', 'm', 'z', 'l'}