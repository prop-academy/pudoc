# React Questions

- What does it mean, when we import something with `{ Component }` in curly braces?
- The react library is split out in the package `React` and `react-dom`. What is handling what?


## package.json

The `^` means, that it will install a newer version of webpack when doint `npm install` again. Example:

```javascript
"devDependencies": {
    "webpack": "^4.29.6"
  }
```

## Webpack

React can be configured via the cli.

## State

- Whenever the state is changed, the render method is called from the component

## Components

- higher order components (??)
- containers (that contain state)
- components (contain props), are arrow function (lowercase) that props are passed as argument (dump component).
- most of the time, you have to make a copy of the state and then return it. Reminder: shallow copy: `newState = [...this.state]`
