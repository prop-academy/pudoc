'use strict';

function random(min, max) {
    var num = Math.floor(Math.random() * (max - min + 1)) + min;
    return num;
}

class Game {
    constructor(canvas) {

        this.canvas = canvas;
        const scaleFactor = 1;
        this.ctx = canvas.getContext('2d');
        this.width = canvas.clientWidth / scaleFactor;
        this.height = canvas.clientHeight / scaleFactor;
        this.ctx.scale(scaleFactor, scaleFactor);


        this.marginTop = 20;
        this.marginSide = 20;
        this.backgroundColor = 'white'

        this.playerLives = 5;

        this.blocksPerLine = 15;
        this.blockLines = 5;
        this.blockHeight = 20;
        this.blocks = [];

        this.paddleColor = '#2f1d1d'
        this.paddleWidth = 120;
        this.paddleHeight = 10;
        this.ballRadius = 7;

        this.ballSpeed = 7;
        this.paddleSpeed = 20;
        
        this.registerButtons();
        this.createBlocks();
        this.initPaddlePos = this.getInitialPaddlePos();
        
        this.paddle = new Paddle(
            this.ctx, this.width, this.height, "", "", this.paddleSpeed, this.initPaddlePos, this.paddleWidth, this.paddleHeight, this.paddleColor);
            this.ball = new Ball(this.ctx, this.width, this.height, 'red', this.ballRadius, this.ballSpeed, this.paddle, this.blocks, this.playerLives);
            
        this.IntervalId = null;

        // Initial rendering before start
        this.render();
        this.ball.gradient();

    }

    getInitialPaddlePos(){
        return [this.width / 2 - this.paddleWidth / 2, this.height - 2 * this.paddleHeight];
    }

    createBlocks() {
        let i = 0;
        let blockWidth = (this.width - 2 * this.marginSide) / this.blocksPerLine;
        let blockHeight = this.blockHeight;
        for (i; i < this.blockLines; i++) {
            let j = 0;
            for (j; j < this.blocksPerLine; j++) {

                let block = new Block(this.ctx, blockWidth, blockHeight, [j * blockWidth + this.marginSide, i * blockHeight + this.marginTop], 'green');
                block.color = 'rgb(' + random(0, 255) + ',' + random(0, 255) + ',' + random(0, 255) + ')';
                this.blocks.push(block);
            }
        }
    }

    renderBlocks() {
        this.blocks.forEach(block => {
            block.render();
        })
    }

    resetCanvas() {
        this.ctx.fillStyle = this.backgroundColor;
        this.ctx.fillRect(0, 0, canvas.width, canvas.height);
    }

    registerButtons() {
        this.startButton = document.getElementById('start')
        this.pauseButton = document.getElementById('pause')
        this.resetButton = document.getElementById('reset')

        this.startButton.onclick = () => this.play()
        this.pauseButton.onclick = () => this.pause()
        this.resetButton.onclick = () => this.reset()
    }

    renderLives() {
        this.ctx.font = "50px Arial";

        // this.ctx.fillStyle = 'rgb(' + random(0, 255) + ',' + random(0, 255) + ',' + random(0, 255) + ')';
        // this.ctx.fillStyle = 'black';

        this.ctx.fillText("\u2665 " + this.ball.lives, this.width / 2 -30 , this.height / 2);
    }

    play() {
        let loop = () => {
            this.render();
            // this.ball.paddleDetect();
            this.ball.move();
            this.ball.collisionDetect();
            if (this.ball.lives === 0) {
                alert('Game Over!')
                this.reset();
                return;
            }

            if (this.ball.blockCounter === 0) {
                alert('You have won the game')
                this.reset();
                return;
            }

            this.IntervalId = requestAnimationFrame(loop);
        }

        if(!this.IntervalId) loop();
    }

    pause() {
        cancelAnimationFrame(this.IntervalId);
        this.IntervalId = null;
    }

    reset() {
        // console.log('reset')
        this.pause();
        this.blocks = [];
        this.createBlocks();
        this.ball.lives = this.playerLives;
        this.ball.blocks = this.blocks;
        this.ball.blockCounter = this.blocks.length;
        this.paddle.width = this.paddleWidth;
        this.paddle.position = this.initPaddlePos;
        // this.paddle.position = this.initPaddlePos;
        this.ball.resetBall();
        this.render();
        // game = new Game(canvas);
    }

    render() {
        this.resetCanvas();
        this.renderBlocks();
        this.renderLives();
        // this.ball.render();
        this.ball.gradient();
        this.paddle.render();
    }
}
