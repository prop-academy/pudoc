# Pytest eggs

Remove `*.pyc*` and `__pychache__`:

- `find -name *.pyc -delete`
- `find . -type d -name "__pycache__" -exec rm -rf {} +`