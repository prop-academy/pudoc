
function binarySearch(L, q) {
    if (!Array.isArray(L) || L.length === 0) return -1;
    let startInd = 0;
    let stopInd = L.length - 1;

    while (startInd <= stopInd){
        let center = (stopInd - startInd) % 2 === 0 ? (stopInd - startInd) / 2 + startInd : (stopInd + 1 - startInd) / 2 + startInd;
        if (L[center] === q) return center;
        if (q > L[center]) {
            startInd = center + 1;
        } else {
            stopInd = center - 1;
        }
    }
    return -1

    
}



console.log(binarySearch([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 2)) // 1
console.log(binarySearch([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 1)) // 0
console.log(binarySearch([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 7)) // 6
console.log(binarySearch([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 10)) // 9