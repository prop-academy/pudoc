function randomInt(max, min = 0) {
    let num = Math.floor(Math.random() * (max - min + 1)) + min;
    return num;
}

let names = ['spanky', 'spongy', 'kuddly']

class Student {
    constructor(name, major) {
        this.name = name;
        this.major = major;
        this.laziness = (Math.random() * (5 - 0 + 1)).toFixed(2);
    }
    greet() { console.log('Wasaaaap broda??') }
}

class Freshman extends Student {
    constructor(name, major) {
        super(name, major)
        this.nickname = this.getNickname()
    }

    getNickname() {
        let rn1 = randomInt(names.length - 1)
        let rn2 = randomInt(names.length - 1)
        const name = names[rn1] + this.major + names[rn2];
        return name;
    }

    party() {
        let noBeers = randomInt(this.nickname.length);
        console.log(`${this.nickname} maybe drank ${noBeers} of beers.`)
    }
}

class Sophomore extends Freshman {
    constructor(name, major) {
        super(name, major)
    }

    impress() {
        let someArrayFromTheBottom = [1,2,3]
        someArrayFromTheBottom.forEach(el => {console.log(`${el}\n`)})
    }
}


astudent = new Student('Charlie', 'Computer Science')
console.log('Laziness: ', astudent.laziness)

freshman = new Freshman('Johnny', 'Biology')
freshman.party()

sophomore = new Sophomore()
// console.log(sophomore.impress())

// stopping here......is becomes repetitive





