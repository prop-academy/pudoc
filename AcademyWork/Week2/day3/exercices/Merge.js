
function mergeObjects(...objs) {
    let newObj = {};
    for (obj of objs) {
        Object.keys(obj).forEach(key =>{
            if(Object.keys(newObj).indexOf(key) === -1) {
                newObj[key] = obj[key];
            }
        })
    }
    return newObj;
}


// let obj2 = { a: 2, c: 4 };
console.log(mergeObjects({ a: 3, b: 2 }, { a: 2, c: 4 }, { e: 8, c: 5})); // { a: 3, b: 2, c: 4, e: 8 })
