
// Aim: All 3 operations push and pop should be equally fast

// the top of the stack is the end of the list

class Stack {

    constructor() {
        this.items = [];
    }


    // Pushes a new element on top of the stack
    push(element) {
        this.items[this.items.length] = element;
    }

    // Removes the top most element from the stack and returns that element
    pop() {
        
        let r = this.items[this.items.length - 1]
        // this.items[this.items.length - 1] = undefined;
        delete this.items[this.items.length - 1]
        this.items.length -= 1;
        return r;
    }

    // Returns the top-most element, but doesn't change the stack
    top () {
        return this.items[this.items.length - 1];
    }

    // Returns true if stack has no elements in it, otherwise false
    isEmpty () {
        return this.items.length === 0;
    }

    // Removes all elements from the stack
    clear () {
        this.items = [];
    }
}

stack = new Stack();

console.log(stack.isEmpty()) // true
stack.push(1); 
console.log(stack.top()) // 1
stack.push(2);
console.log(stack.top()) // 2
let val = stack.pop();
console.log(val); // 2
console.log(stack.top()) // 1
stack.clear();
console.log(stack.isEmpty());


