// Write a function that checks if a value is classified as a boolean primitive. 
// Return true or false. Boolean primitives are true and false.

function is_it_true(args) {
    return typeof args === "boolean"
}

console.log(is_it_true(true)) // true
console.log(is_it_true(false)) // true
console.log(is_it_true('true')) // false
console.log(is_it_true(1)) // false
console.log(is_it_true('false')) // false
