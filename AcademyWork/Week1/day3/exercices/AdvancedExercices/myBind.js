// Define a myBind function that simulates the bind method on Function.

function myBind(func, obj){
    return func.bind(obj);
}

const obj = {
    name: 'Markov'
}

function printName() {
    console.log('Thy name is: ' + this.name);
}

printName();

const boundPrint = myBind(printName, obj);

boundPrint();

