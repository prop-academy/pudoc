/* Write a function that takes two integers. The first one will be the base b and the second one n will be the exponent.

The function should return the value of b raised to the power n.

Try to solve it with recursion.

You can try it with a while loop first and then try to implement the recursive approach. */

function expWhile(base, exp) {
    let result = 1;
    let i = 0;
    while (i < exp) {
        result *= base;
        i++;
    }
    console.log(result)
    return result;
}

function exp(base, exp) {

    let i = 0;
    let result = 1;

    function multiply() {
        if (i < exp) {
            result *= base
            i++;
            multiply();
        }
    }
    multiply(base, base);
    console.log(result);
    return result;
}



exp(5, 3); // => 125
exp(2, 4); // => 16
exp(5, 1); // => 5
exp(6, 0); // => 1
