Use a snap install instead of using `apt`. Read here about [snaps](https://docs.snapcraft.io/getting-started/3876).

    snap search pycharm
    snap search vscode
    snap sarch atom
    
    sudo snap install pycharm-community --classic
    sudo snap install vscode --classic
    # sudo snap install atom --classic
    
    # Cool git managing tool done with electron.js
    # sudo snap install gitkraken