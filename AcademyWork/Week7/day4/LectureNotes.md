# Redux and State

## Reducer function

- pure function, input: `state`,`action` in that order.
- the redux state is in the store
- the dispatch method calls the reducer function

## Common practices

- intial state for reducers

## React and Redux

- combine reducers
- the former state of an app is now passed as props coming from redux state.
- `mapStateToProps`: introduces the logic how the data (data structure can be changed) from the state
is handed over as props to the the child component/container.


## Questions

- When you pass props to a container or smart component, does it merge the props passed from redux with the props
passed by another parent component?