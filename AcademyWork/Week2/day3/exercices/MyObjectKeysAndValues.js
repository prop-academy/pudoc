function objectKeys(obj) {
    let r = [];
    for (keys in obj) r.push(keys);
    return r;
}


function objectValues(obj) {
    let r = [];
    for (keys in obj) r.push(obj[keys]);
    return r;
}

let a = { a: 1, b: 4, c: 7, e: 12 };

console.log(objectKeys(a));
console.log(objectValues(a));
