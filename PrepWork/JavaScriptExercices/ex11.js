// Repeat a given string (first argument) num times (second argument). 
// Return an empty string if num is not a positive number.

function repeat_string_num_times(str, num) {
    let r = '';
    if (!Number.isInteger(num) || num < 0) return ''

    while (num--) {
        r += str;
        if (num === 0) break;
    }
    return r
}

console.log(repeat_string_num_times("abc", 3)) // 'abcabcabc'
console.log(repeat_string_num_times("abc", 1)) // 'abc'
console.log(repeat_string_num_times("abc", 0)) // ''
console.log(repeat_string_num_times("abc", -1)) // ''
