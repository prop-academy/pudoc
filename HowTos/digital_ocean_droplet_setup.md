# Digital Ocean setup for docker

Links:
- https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04
- https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04
- https://docs.gitlab.com/runner/install/

script running as root:

```bash

# Add user with password defined
adduser dev
# Add user to the sudo group
usermod -aG sudo dev

su - dev
sudo ls -la /
exit

# Copy ssh keys as user root
cd
mkdir -p /home/dev/.ssh
cp /root/.ssh/authorized_keys /home/dev/.ssh/authorized_keys
chown -R dev:dev /home/dev/.ssh/
# Logout from server
exit

```
Logout and login with ssh dev@....
If successfull:

```bash

sudo rm /root/.ssh/authorized_keys
# Disable root login
sudo nano /etc/ssh/sshd_config

# Change this line
permitRootLogin no

```
Configurations Script:  
Run with sudo and create it first:

```bash

nano install_script.sh

```
Use `Ctrl+O` to save and `Ctrl+x` to exit.

```bash
#!/usr/bin/env bash
set -ex

#
# Check for root
#
if [ "$(id -u)" != "0" ]
then
    errorecho "ERROR: This script has to be run as root!"
    exit 1
fi

# Install uncomplicated firewall
apt update && apt upgrade -y
apt-get install ufw
ufw default deny incoming
ufw default allow outgoing
ufw allow 22/tcp
ufw allow 80/tcp
ufw allow 443/tcp

echo "y" | ufw enable
ufw status

# Install docker from official digital ocean guides
apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
# add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt update
apt-cache policy docker-ce
apt install -y docker-ce
#systemctl status docker

gpasswd -a $SUDO_USER docker

docker run hello-world

# Install docker compose, change to current version
DC_VERSION="1.23.2"
curl -L https://github.com/docker/compose/releases/download/$DC_VERSION/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose --version

docker rmi -f hello-world

# Install gitlab runner
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash
apt-get install gitlab-runner
gpasswd -a gitlab-runner docker

```

Create a file `install.sh` with the code above and do `sudo chmod u+x install.sh` and
run it wiht `sudo ./install.sh`

Install certbot:  

```bash
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install -y certbot
sudo certbot certonly --standalone --preferred-challenges http -d anavys-back.propulsion-learn.ch
```