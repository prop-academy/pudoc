/* 11. Search and Replace
You will create a program that takes a sentence, then search for a word in it and 
replaces it for a new one while preserving the uppercase if there is one. */


function myReplace(sentence, query, replaceWith) {
    patt = new RegExp(query);
    let match = sentence.match(patt);
    if (!match) {
        console.log('No match - returning input sentence');
        return sentence;
    };
    matchIndex = match['index'];
    let repl = replaceWith;


    // check if the first letter of the word is uppercase
    let firstLetter = sentence[matchIndex];
    if (firstLetter === firstLetter.toUpperCase()) {
        repl = replaceWith[0].toUpperCase() + replaceWith.slice(1);
        console.log(repl);
    }

    let result = sentence.replace(patt, repl);
    console.log(result);
}


myReplace("He is Sleeping on the couch", "sleeping", "sitting"); // "He is Sitting on the couch"
