/* Time to try out some different options to create objects.

Here are the specifications for our Event ‘object creator’.

    Fields
        location, a string
        date, a date
        assistants, an array of names
    Methods:
        printInvitation(name) that expects the name of a person and prints a message with information about the event.
        confirmAssistance(name) that pops a message in the browser and asks the user to confirm his/her assistance. In that case his/her name will be added to the array of assistants.
        printAssistants() that prints all assistants in a new line.

Implement the previous Object Creator with each of these patterns:

1. Constructor pattern

    const event1 = new Event('Zürich', new Date(2025, 11, 17), ['Llorenç', 'Colin', 'Laurent']);

2. OLOO

    const event2 = Object.create(EventOLOO);
    event2.init('Zürich', new Date(2025, 11, 17), ['Llorenç', 'Colin', 'Laurent']);

3. Factory pattern

    const  event3 = eventFactory('Zürich', new Date(2025, 11, 17), ['Llorenç', 'Colin', 'Laurent']);

Check that all your implementations work properly by creating some instances and adding values to their fields.

*/

// DRY !!!!!

const confirmInvitation = function (name) {
    console.log(
        `Hi ${name}\nYou are kindly invited to join our event in ${this.location} on ${this.date}`
    )
}

const confirmAssistance = function (name) {
    let input = prompt(`Will you assist in ${this.location} on ${this.date}?\nIf yes, enter your name, otherwise hit cancel!`)
    if (input) this.assistants.push(input)
}

const printAssistants = function () {
    this.assistants.forEach(el => { console.log(`${el}\n`) })
}

// -------- 1 Constructor pattern
// ES2015

const Event = function (location, date, assistants) {
    this.location = location;
    this.date = date;
    this.assistants = assistants;
}

Event.prototype.confirmInvitation = confirmInvitation;

Event.prototype.confirmAssistance = confirmAssistance;

Event.prototype.printAssistants = printAssistants;

const event1 = new Event('Zürich', new Date(2025, 11, 17), ['Llorenç', 'Colin', 'Laurent']);
console.log(event1.printAssistants())


// -------- 2 OLOO (Objects Linked to Other Objects)

const EventOLOO = {
    init: function (location, date, assistants) {
        this.location = location;
        this.date = date;
        this.assistants = assistants;
    },

    confirmInvitation: confirmInvitation,
    confirmAssistance: confirmAssistance,
    printAssistants: printAssistants

}

const event2 = Object.create(EventOLOO);
event2.init('Zürich', new Date(2025, 11, 17), ['Llorenç', 'Colin', 'Laurent']);
console.log(event1.printAssistants())

// -------- 3 Object Literal 

/* Create a plain object every time you need one. */

const messi = {
    legs: 2,
    talk: function () {
        console.log('Talking');
    },
    score: function () {
        console.log('Goal!');
    },
    name: 'Messi'
};
// --> dont need an object right now =)) ....break; ....coffeee ....continue;


// ---------  4 Object.create

// You can also use Object.create as we have seen earlier, but not the same:

const person = {
    legs: 2,
    talk: function() {
      console.log('Talking...');
    }
  };
  
  const messi = Object.create(person);
  messi.name = 'Messi';
  messi.score = function() {
    console.log('Goal!!');
  }

