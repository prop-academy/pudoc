

```html

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">  
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    
</body>
</html>

```

Some notes:

- don't use `</br>`
- span is similar to div in its meaning, but inline
- `section` element comes from the print media idea to have articles that have sections
- block elements always take 100% of the element.

## CSS

- Priority: id, class and then DOM element. This is called specificity.
- position allows to position an element very extact with `left`, `right`, `top`, etc.
- `position: fixed/absolute;` is good to add static navs at the top. Then use the `z-index` to manage conflicts between overlapping elements.
- `position: fixed;` will always display the element at the first pixel visible to the user, so scrolling is not affecting the element.
- `display` property

## Flexbox

- new, like 4-5 years.
- main axis is x with flex-direction row and justify is used for the main axis.
- when choosing flex-direction to row, the main axis becomes the y-axis. then justify acts in Y-Direction.

## CSS-Grids

- very new, like 2 years ago

## Bootstrap

- does not use for his grids CSS grids. The grid system is implemented in a InternetExplorer-11-friendly manner.

## Ressources

- [Can I use it?](https://caniuse.com/) 
- [BEM Block Element Modifier by Yandex](https://tech.yandex.com/bem/)
- [Storybook](https://storybook.js.org/)
- [DocZ](https://www.docz.site/)
- [React Style GuidDist](https://github.com/styleguidist/react-styleguidist)

## The future

- Pattern libraries (like Bootstrap) based on atomic design.