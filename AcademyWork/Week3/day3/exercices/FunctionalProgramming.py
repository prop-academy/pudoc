# Even Numbers
# Write a program which can filter even numbers in a list by using filter function. The list is: [1,2,3,4,5,6,7,8,9,10].
from functools import reduce

filter_even = lambda alist: filter(lambda x: x%2 == 0, alist)
print(list(filter_even([1,2,3,4,5,6,7,8,9,10])))


# Square Elements
# Write a program that accepts a list and returns the square of each element. The list is: [1,2,3,4,5,6,7,8,9,10].

square_elements = lambda alist: map(lambda x: x*x, alist)
print(list(square_elements([1,2,3,4,5,6,7,8,9,10])))

# Squared Even Numbers
# Write a program which uses map() and filter() to make a list of elements that are the squares of the even numbers in [1,2,3,4,5,6,7,8,9,10].

square_even_number = lambda alist: map(lambda x: x*x, filter_even(alist))
print(list(square_even_number([1,2,3,4,5,6,7,8,9,10])))

# Find Certain Numbers
# Write a function find_numbers(min, max) that will find all numbers that are a multiple of 7 but not a multiple of 5.
# First, try getting it right with an explicit for loop. If you have it up and running, write a second version that
# avoids explicit for loops and uses filter and lambda only!


find_number = lambda min, max: filter(lambda x: x % 7 == 0, \
                                             filter(lambda x: x % 5 != 0, \
                                                    filter(lambda x: x > min and x < max, range(min, max))))

print(list(find_number(min=0, max=55)))

# ONLINE SHOP

orders = [

    {
        'id': 'order_001',
        'item': 'Introduction to Python',
        'quantity': 1,
        'price_per_item': 32,
    },
    {
        'id': 'order_002',
        'item': 'Advanced Python',
        'quantity': 3,
        'price_per_item': 40,
    },
    {
        'id': 'order_003',
        'item': 'Python web frameworks',
        'quantity': 2,
        'price_per_item': 51,
    },
]

calc_amount = lambda obj: obj['quantity'] * obj['price_per_item']
punish_customer = lambda price: price if price > 100 else price + 10
compute_totals = lambda obj_list: map(lambda obj: (obj['id'], punish_customer(calc_amount(obj))), obj_list)

totals = compute_totals(orders)
print(list(totals))

# Input and Range
# Given an integer n, write a program that generates a dictionary with entries from 1 to n. For each key i,
# the corresponding value should be i*i.

squared_dict = lambda num: {str(num): num*num for num in range(num)}    # this time, no generators

# List Comprehension
# Using List comprehension generate a list where the values are the squares of 1 to 20 (both included). Then print the last 5 elements in the list.

square_list = [i*i for i in range(1,21)]
print(square_list[-5:])

# Example CUM sum
cum_sum = reduce(lambda x, y: x + y, [1, 2, 3, 4, 5])

# Print Style
# Write a small script that will print numbers 1-10 in a way that if the number is even it prints the number, if the number is odd it print underscore _:

special_print = lambda num_list: reduce(lambda x,y: str(x) + str(y) if y%2 == 0 else str(x) + '_', num_list)
print(special_print([i for i in range(11)]))


