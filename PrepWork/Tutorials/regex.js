// In JavaScript, the RegExp object is a regular expression object with predefined properties and methods.


/*
Regular Expression Modifiers

Modifiers can be used to perform case-insensitive more global searches:
Modifier 	Description 	Try it
i 	Perform case-insensitive matching
g 	Perform a global match (find all matches rather than stopping after the first match)
m 	Perform multiline matching*/

var str = "Visit W3Schools";
var n = str.search(/w3schools/i);   // returns index where match starts

// using string replace with a term
var str = "Visit Microsoft!";
var res = str.replace("Microsoft", "W3Schools");

// Use a case insensitive regular expression to replace Microsoft with W3Schools in a string:
var str = "Visit Microsoft!";
var res = str.replace(/microsoft/i, "W3Schools");


Using test()
/*
The test() method is a RegExp expression method.
It searches a string for a pattern, and returns true or false, depending on the result.
The following example searches a string for the character "e":*/

var patt = /e/;
patt.test("The best things in life are free!");   // returns true
 /e/.test("The best things in life are free!");

 /*
 Using exec()

The exec() method is a RegExp expression method.
It searches a string for a specified pattern, and returns the found text as an object.
If no match is found, it returns an empty (null) object.
The following example searches a string for the character "e":
 */
/e/.exec("The best things in life are free!");
