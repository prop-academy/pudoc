# The O-Notation

Most of the time the most important relation is denoted, so the O-Notation of the space-complexity if it is predominant, or the time-complexity.
Example: `4n2 + 3n + x` becomes `O(n²)`.

# Selection Sort Algorithm

The selection sort algorithm has a finite list of `n` numbers as input and returns a sorted list of same length.

        L = L // undsorted list
        L_sorted = [] // the already sorted portion of the array
        
The algorithm looks for the smallest value in the unsorted list, pop's it and appends it to the end of the list.

### O() memory

1. It writes out `n` values to a new list. `O(n)`

### O() runtime steps

1. going `n` times through `L` and writing to the now.
2. `n` time find the smallest value in the remaining unsorted array, counting the finding of the value as one step:  that is `n * (n+1)/2`.

That would result is `n * (n+1) / 2` that would lead to `O(n²)`.

# Merge-Sort Algorithm

Splits the input list in two halfs `0:n/2` and `n/2:n`. It splits this list again until it can compare. The two sorted lists are merged with a `mergeList` function, that compares `A[0] < B[0]` and if pops the correpsonding value out of A or B and pushes it to the end of the result list.

## merge part of it

1. The merge part makes `n-1` comparisons and writes `n` times in memory in the worst case where `n` is the both array lengths together.

## Recursive list splitting and comparing. 

1. The program will split the list in two and the recursively until the list contains just two items. There are `n/2` sublists todo a value comparison. The splitting in sublists is mainly accessing memory.
2. `n/2` sublists are merged `n/4` times with `merge(l1,l2)` where `l1` and `l2` contain two items. This makes 3 steps (`S`) and 4 memory writes (`M`) for each merge. So `n/4 * ((4-1)S + 4M)`. Now `n/4` lists are remaining.
4. The amount of time merge is called is `log2(n)`. The memory writes and steps: the first time merge process (4-1) steps, next time, (8-1), then 15 `2^log2(n) = n` in the last step. I would guess its the writes and steps are somehow related like `going from 2² to n or from 2² to 2^log2(n)`. At the end, this might just become also `n`.
5. Finally I would guess it will be `n * log2(n)` for the writes and steps since every step there was one comparison less than writes.
6. Wikipedia: in terms of Memory it is `O(n)`.
 
