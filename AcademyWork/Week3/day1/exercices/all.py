import re

def is_string(string):
    return isinstance(string, str)

def is_only_string(string):
    if not isinstance(string, str): return False
    pattern = r"[\d+|\s+]"
    found = re.search(pattern, string)
    if found: return False
    return True

# print(is_only_string('11'))
# print(is_only_string('hello'))
# print(is_only_string(['hello']))
# print(is_only_string('this is a long sentence'))
# print(is_only_string({'a': 'this is a long sentence'}))

def is_alphanumeric(string):
    if not isinstance(string, str): return False
    pattern = r"\d*"
    return len(re.search(pattern, string)[0]) == len(string)

# print(is_alphanumeric('1234'))
# print(is_alphanumeric('12 4'))

def is_array_or_tuple(obj):
    if isinstance(obj, list) or isinstance(obj, tuple): return True
    return False

# print(is_array_or_tuple(['list']))
# print(is_array_or_tuple((1,32,'tuple')))
# print(is_array_or_tuple('string'))

def same_type(obj1, obj2):
    return isinstance(obj1, type(obj2))

# print(same_type([], ['1']))
# print(same_type('', ['1']))
# print(same_type((), ()))

def intersection(string1, string2):
    r = []
    all_str = string1 + string2

    for l in all_str:
        if not l in r:
            r.append(l)
    r.sort()
    return "".join(r)

# a = 'xyaabbbccccdefww'
# b = 'xxxxyyyyabklmopq'
# x = 'abcdefghijklmnopqrstuvwxyz'
# print(intersection(a, b))  # abcdefklmopqwxy
# print(intersection(a, x))  # abcdefghijklmnopqrstuvwxyz

def convert(rand_num):
    new_list = [s for s in str(rand_num)]
    new_list.sort(reverse=True)
    return new_list

# print(convert(429563))  # [9, 6, 5, 4, 3, 2]
# print(convert(324))     # [4, 3, 2]

def count_repetition(string_list):
    counter_dict = {}

    for string in string_list:
        if string in counter_dict.keys():
            counter_dict[string] += 1
        else:
            counter_dict[string] = 1

    return counter_dict


names = ['kerouac', 'fante', 'fante', 'buk', 'hemingway', 'hornby', 'kerouac', 'buk', 'fante']
# {'kerouac': 2, 'fante': 3, 'buk': 2, 'hemingway': 1, 'hornby': 1}
print(count_repetition(names))

def is_caught(string):
    cat_pos = re.search(r'C', string).regs[0][0]
    mouse_pos = re.search(r'm', string).regs[0][0]
    if mouse_pos - cat_pos <= 3: return True
    return False

# print(is_caught('C.....m'))   # False
# print(is_caught('C..m'))      # True
# print(is_caught('..C..m'))    # True
# print(is_caught('...C...m'))  # False
# print(is_caught('C.m'))       # True


def my_mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)

def split_the_bill(group_dict):
    vals = group_dict.values()
    mean_amount = my_mean(list(vals))
    print(mean_amount)
    for name, amount in group_dict.items():
        group_dict[name] = mean_amount - amount
    return group_dict

# group = {
#     'Amy': 20,
#     'Bill': 15,
#     'Chris': 10
# }
#
# print(split_the_bill(group))


