// Write a method that takes an array of numbers. If a pair of numbers in the array sums to zero,
//  return the positions of those two numbers. If no pair of numbers sums to zero, return null.

// function unique(array){
//     let r = [];
//     for(n in array){
//         if(r.indexOf(num[n]) === -1) r.push(num[n])
//     }
//     return r
// }

function twoSum(nums) {
    if (!nums.constructor === Array) return 'Only array are accepted as input.'
    let resultList = [];
    // Only same numbers with different signs add up to 0
    for (ind in nums) {
        let num = nums[ind];
        let complement = nums.indexOf(-num, ind);
        // console.log('comp:', complement)
        if (complement !== -1) {
            resultList.push([parseInt(ind), complement]);
        }
    }
    if (resultList.length === 0) return null
    return resultList
}

console.log(twoSum([1, 3, -1, 5]))
console.log(twoSum([1, 3, -1, 5, -3]));
console.log(twoSum([1, 5, 3, -4]));