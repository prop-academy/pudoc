import React, { Component } from 'react'

const buttonStyle = {
    backgroundColor: 'green'
}

class App extends Component{

    state = {
        name: 'NAMES',
        counter: 0,
        students: [
            {name: 'Vidal', age: 32},
            {name: 'John', age: 62},
        ],
        showStudents: true,
    }

    onCounterInc = () => {
        const newCounter = this.state.counter += 1;
        this.setState({
            counter: newCounter
        })
    }

    onCounterDec = () => {
        const newCounter = this.state.counter -= 1;
        this.setState({
            counter: newCounter
        })
    }

    onShowStudents = () => {
        const newShowStudents = !this.state.showStudents
        this.setState({
            showStudents: newShowStudents
        })
    }

    render(){
        return (
            <div>
                <h3>Hello from a var called {this.state.name}</h3>
                <h3>Counter: {this.state.counter}</h3>
                <button onClick={() => this.onCounterInc()}>Count up</button>
                <button onClick={() => this.onCounterDec()}>Count down</button>
                <button onClick={() => this.onShowStudents()}>Show/Hide Students</button>
                {this.state.showStudents ? this.state.students.map(student =>{
                    return (
                        <div className='container'>
                            <span>{student.name}</span>
                            <span>{student.age}</span>
                        </div>
                    )
                }): null}
            </div>
        )
    }
}

export default App;