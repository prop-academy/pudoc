// Take a random number and convert it to a reverse-sorted array of digits.

function convert(num) {
    let numString = num.toString();
    let splitList = numString.split('');
    let splitNumList = splitList.map(elem => parseInt(elem));
    let final = splitNumList.sort().reverse();
    console.log(final);
    return final;
}

convert(429563) // => [9, 6, 5, 4, 3, 2]
convert(324) // => [4, 3, 2]