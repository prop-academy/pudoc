class Paddle {
    constructor(x, canvas, keyUp, keyDown, paddleVel) {
        this.x = x;
        this.y = canvas.height / 2;
        this.canvasWidth = 10;
        this.height = 70;
        this.paddleVelocity = paddleVel;
        this.color = 'white';
        this.keyUp = keyUp || 'ArrowUp';
        this.keyDown = keyDown || 'ArrowDown';
        this.getObstacleCoordinates();
        document.addEventListener('keydown', event => {this.move(event, canvas.height); });
    }

    move(e, canvasHeight) {
        if (e.key === this.keyUp && this.y > 0) {
            this.y -= this.paddleVelocity;
            this.getObstacleCoordinates()
        }
        if (e.key === this.keyDown && this.y < canvasHeight) {
            this.y += this.paddleVelocity;
            this.getObstacleCoordinates()
        }
    }

    getObstacleCoordinates(){
        this.x1 = this.x;
        this.y1 = this.y;
        this.x2 = this.x + this.canvasWidth,
        this.y2 = this.y + this.height
    }

    render(ctx) {
        ctx.beginPath()
        ctx.fillStyle = this.color
        ctx.rect(this.x, this.y, this.canvasWidth, this.height);
        ctx.fill();
    }
}
