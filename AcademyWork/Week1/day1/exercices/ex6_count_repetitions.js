// You are given an array of strings (authors). Count the number of times each string occurs and keep track of it in a dict.

let authors = ['kerouac', 'fante', 'fante', 'buk', 'hemingway', 'hornby', 'kerouac', 'buk', 'fante']

// let r = [];
//     for (arg of hugeList) {
//         if (r.indexOf(arg) === -1) r.push(arg);
//     }
//     let finalString = r.sort().join('');

function valueCount(arr) {
    let countDict = {};

    arr.forEach(key => {
        if (key in countDict) countDict[key] += 1;
        else countDict[key] = 1;
    });
    console.log(countDict);
    return countDict;
}

valueCount(authors);

/* {
    kerouac: 2,
    fante: 3,
    buk: 2,
    hemingway: 1,
    hornby: 1
  } */
  