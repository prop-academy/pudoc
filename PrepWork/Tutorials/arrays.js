// empty array
var points = new Array()    // bad
var points = [];            // good

// Automatic toString()

//JavaScript automatically converts an array to a comma separated string when a primitive value is expected.
//This is always the case when you try to output an array.


fruits.toString();
console.log(fruits);

//The easiest way to add a new element to an array is using the push method:

var fruits = ["Banana", "Orange", "Apple", "Mango"];
// Push also returns the new array length
length = fruits.push("Lemon");    // adds a new element (Lemon) to fruits
// Remove last item with pop
fruits.pop("Lemon");    // adds a new element (Lemon) to fruits
// Shifting is equivalent to popping, working on the first element instead of the last.
// returns the deleted element
first_entry  = fruits.shift();            // Removes the first element "Banana" from fruits
newlen = fruits.unshift("Lemon");    // Adds a new element "Lemon" to fruits

var points = new Array(40);  // Creates an array with 40 undefined elements !!!!!
var points = [40,];

var points = [40,20,100];
points.sort();  // produces the wrong results, since sort is used for strings
points.reverse();       // reverse the order of the elements

delete points[1];   // first element will be undefined


// using length property
var fruits = ["Banana", "Orange", "Apple", "Mango"];
fruits[fruits.length] = "Lemon";    // adds a new element (Lemon) to fruits

// How to Recognize an Array

var fruits = ["Banana", "Orange", "Apple", "Mango"];
typeof fruits;    // returns object
fruits instanceof Array;   // returns true

// ES5, but not older
Array.isArray(fruits);   // returns trues

// Slicing
// The slice() method creates a new array. It does not remove any elements from the source array.
var fruits = ["Banana", "Orange", "Lemon", "Apple", "Mango"];
var citrus = fruits.slice(3);       // returns [Apple, Mango], slices the rest of the array
var citrus = fruits.slice(1, 3);    // returns [Orange, Lemon]

// Splicing an array
// The splice() method can be used to add new items to an array:

var fruits = ["Banana", "Orange", "Apple", "Mango"];
fruits.splice(2, 0, "Lemon", "Kiwi");
// start inserting at 2, deleting 0 elements


// Concatenation
var myGirls = ["Cecilie", "Lone"];
var myBoys = ["Emil", "Tobias", "Linus"];
var myChildren = myGirls.concat(myBoys);   // Concatenates (joins) myGirls and myBoys

// ARRAY ITERATION

//Array.forEach()

// The forEach() method calls a function (a callback function) once for each array element.
// Array.forEach() is supported in all browsers except Internet Explorer 8 or earlier:
var txt = "";
var numbers = [45, 4, 9, 16, 25];
numbers.forEach(myFunction);

// index and array inputs can be omitted if not used
function myFunction(value, index, array) {
  txt = txt + value + "<br>";
  console.log(value, index)
}

// Array.map()

/*
    - The map() method creates a new array by performing a function on each array element.
    - The map() method does not execute the function for array elements without values.
    - The map() method does not change the original array.
*/

var numbers1 = [45, 4, 9, 16, 25];
var numbers2 = numbers1.map(myFunction);

function myFunction(value, index, array) {
  return value * 2;
}

// Array.filter()

/*
    The filter() method creates a new array with array elements that passes a test.
    This example creates a new array from elements with a value larger than 18:
*/

var numbers = [45, 4, 9, 16, 25];
var over18 = numbers.filter(myFunction);

// also index and array can be omitted as input args
function myFunction(value, index, array) {
  return value > 18;
}

// Array.reduce()

/*
    The reduce() method runs a function on each array element to produce (reduce it to) a single value.
    The reduce() method works from left-to-right in the array. See also reduceRight().
    The reduce() method does not reduce the original array.
*/

// This example finds the sum of all numbers in an array:
var numbers1 = [45, 4, 9, 16, 25];
var sum = numbers1.reduce(myFunction);

function myFunction(total, value) {
  return total + value;
}

// The reduce() method can accept an initial value:
var numbers1 = [45, 4, 9, 16, 25];
var sum = numbers1.reduce(myFunction, 100);

function myFunction(total, value) {
  return total + value;
}

// Array.every()

// The every() method check if all array values pass a test.


// This example check if all array values are larger than 18:
var numbers = [45, 4, 9, 16, 25];
var allOver18 = numbers.every(myFunction);

function myFunction(value, index, array) {
  return value > 18;
}

// Array.some()

//The some() method check if some array values pass a test.

//This example check if some array values are larger than 18:
var numbers = [45, 4, 9, 16, 25];
var someOver18 = numbers.some(myFunction);

function myFunction(value, index, array) {
  return value > 18;
}

// Array.indexOf()

/*
    Search an array for an element value and returns its position.
    Note: The first item has position 0, the second item has position 1, and so on.
*/

//Search an array for the item "Apple":
var fruits = ["Apple", "Orange", "Apple", "Mango"];
var a = fruits.indexOf("Apple");
// Array.indexOf() is supported in all browsers except Internet Explorer 8 or earlier.

// Array.lastIndexOf()

//Array.lastIndexOf() is the same as Array.indexOf(), but searches from the end of the array.
//Search an array for the item "Apple":
var fruits = ["Apple", "Orange", "Apple", "Mango"];
var a = fruits.lastIndexOf("Apple");
// Array.lastIndexOf() is supported in all browsers except Internet Explorer 8 or earlier.

// Array.find()

// The find() method returns the value of the first array element that passes a test function.

// This example finds (returns the value of ) the first element that is larger than 18:
var numbers = [4, 9, 16, 25, 29];
var first = numbers.find(myFunction);

function myFunction(value, index, array) {
  return value > 18;
}

// Array.findIndex()

// The findIndex() method returns the index of the first array element that passes a test function.

// This example finds the index of the first element that is larger than 18:
var numbers = [4, 9, 16, 25, 29];
var first = numbers.findIndex(myFunction);

function myFunction(value, index, array) {
  return value > 18;
}
// Array.findIndex() is not supported in older browsers.
