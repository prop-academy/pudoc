/* Check if a string (first argument, str) ends with the given target string (second argument, target). 
Please do not use .endsWith() method. Try to use one of the JavaScript substring methods instead.
 */


function confirmEnding(str, ending){
    // check the length of the ending, slice str and compare
    let len = ending.length;
    let strEnd= str.slice(-len);
    if(strEnd === ending) {
        console.log('true');
        return true};
    console.log('false');
    return false;
}

confirmEnding("Open sesame", "same"); // true
confirmEnding("Some other example", "ple"); // true
confirmEnding("Some other example", "pl"); // false
