/*The labels in JavaScript are used mainly with break, or continue in nested
loops to be able to break the outer, or continue the outer loop from the code
inside inner loop:
*/
outer:
for (let i = 0; i < 10; i++)
{
   let k = 5;
   for (let j = 0; j < 10; j++) // inner loop
      if (j > 5)
           break; // inner
      else
           continue outer;  // it will go to next iteration of outer loop
}

// With a label reference, the break statement can be used to jump out of any code block:
var cars = ["BMW", "Volvo", "Saab", "Ford"];
list: {
  text += cars[0] + "<br>";
  text += cars[1] + "<br>";
  break list;
  text += cars[2] + "<br>";
  text += cars[3] + "<br>";
}
