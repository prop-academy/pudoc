/* There are 6 different ways to define a function in javascript */

// simple add function, can also be defined at the bottom
var x = myFunction(4, 3);   // Function is called, return value will end up in x

function myFunction(a, b) {
  return a * b;             // Function returns the product of a and b
}

function myArrayMax(arr) {
  var len = arr.length
  var max = -Infinity;
  while (len--) {
    if (arr[len] > max) {
      max = arr[len];
    }
  }
  return max;
}

function myArrayMin(arr) {
  var len = arr.length
  var min = Infinity;
  while (len--) {
    if (arr[len] < min) {
      min = arr[len];
    }
  }
  return min;
}

function name(a){
  // a is an argument
  console.log(arguments);
}

name('IAN'); // IAN is am parameter


console.log(name('IAN')); // returns Ian and undefined, by default JS returs undefined when not return statement is given


function add(a) {console.log(a)}

console.log(add(4) + 1) // undefined + 1 returns NaN


// Are those two definitions the same?
const hello = function(a) {console.log(a)};
function hello (a) {console.log(a)}

// Recursive function

const number = function b(a){
  // for(let i=a; a<=0; i--){
  //   console.log(i);
  // }

  if(a>=0){
    console.log(a)
    a--;
    return b(a);
  } 
  return number;
};

b('somethin');  // will not work, b is not known, only usecase is when you want to call b inside of b (recursive function)