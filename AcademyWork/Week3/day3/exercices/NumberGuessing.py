# Number Guessing Game
# In this exercise we will build a simple game that lets the user guess a random number.

import random

class GuessNumberGame:

    def start(self):
        print('I am thinking of a number between 1 and 10.\n Can you find it?\nYou have 5 tries')
        rand_num = random.randint(1,11)
        print(rand_num)
        tries = 0
        while True:
            if tries == 5:
                print('You lost the game. You have already tried 5 times.')
                break
            try:
                guess = int(input('Guess what it is: '))
                if guess == rand_num:
                    print(f'Congratulations. You guessed it in {tries} tries.')
                    break
                elif guess < rand_num:
                    print("Nope, its higher than that")
                else:
                    print("Nope, it's lower than that")
                tries += 1
            except ValueError:
                print('please enter a number not something else...')

        self.play_again()

    def play_again(self):
        user_input = None
        while user_input != 'n':
            user_input = input('Play again? (y/n)')
            if user_input == 'y': self.start()


if __name__ == '__main__':
    game = GuessNumberGame()
    game.start()