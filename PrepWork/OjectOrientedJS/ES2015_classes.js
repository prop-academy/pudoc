

class Person {
    constructor(first, last, age, gender, interests) {
      this.name = {
        first,
        last
      };
      this.age = age;
      this.gender = gender;
      this.interests = interests;
    }
  
    greeting() {
      console.log(`Hi! I'm ${this.name.first}`);
    };
  
    farewell() {
      console.log(`${this.name.first} has left the building. Bye for now!`);
    };
  }

 let person1 = new Person('Marc', 'Johnsson', 44, 'male', ['football', 'tennis']);
 let han = new Person('Han', 'Solo', 25, 'male', ['Smuggling']);

 console.log(person1.greeting());

class Teacher extends Person {
    constructor(first, last, age, gender, interests, subject, grade) {

        // this.name = {
        //     first,
        //     last
        // };

        // this.age = age;
        // this.gender = gender;
        // this.interests = interests;

        // Code above can be simplified with a super call to the parent constructor
        super(first, last, age, gender, interests);    

        // teacher specific

        this._subject = subject;
        this.grade = grade;        
    }
    
    get subject() {
      return this._subject;
    };

    set subject(newSubject){
      this._subject = newSubject;
    }
};

let snape = new Teacher('Severus', 'Snape', 58, 'male', ['Potions'], 'Dark arts', 5);


console.log('Object.getOwnPropertyNames(Teacher.prototype): ', Object.getOwnPropertyNames(Teacher.prototype));
console.log('Object.getOwnPropertyNames(Person.prototype): ', Object.getOwnPropertyNames(Person.prototype));
console.log('Object.getOwnPropertyNames(Person.prototype.constructor): ', Object.getOwnPropertyNames(Person.prototype.constructor));

console.log(snape.farewell());
console.log('snape.constructor.prototype:  ', snape.constructor.prototype);


snape.subject = 'Black Arts';

console.log('snape has ne subject: ', snape.subject)