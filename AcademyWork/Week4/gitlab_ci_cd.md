# Questions

- what would you use tags for?
- what would you use docker labels for?
- in our example:  did we tag latest for our release candidate and the production?
- how do i build my images locally with docker compose / docker when I use `image: registry....`?
- why do I do the pull stage? On my development machine or the server respectively, there are the caches from
the last build?
- when I does docker pull, does it download all layer or are there cached layers on the system?
- every stage does not know the results from the previous, is this also true for caching of layers?


# use case:

- 1 backend application using docker compose with a database, nginx and django container. For this I do a repo
separately for django and nginx. They have dependecies like variables they share (nginx server config and the hostname
of the container defined in docker-compose file.)
- Since we call docker build ., I assume that the code base is updated from gitlab and then built. 
- what advantages do I have to use the gitlab registry?
- what is the workflow now with gitlab having tagged my images?
- what happens if I push 10 commits? Does it start the pipe line 10 times? It build per push and uses
the latest commit ref to tag it.