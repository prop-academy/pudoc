let l1 = [2, 4, 6, 8, 10];
let l2 = [1, 3, 5, 7, 9];

// for ascending ordered list, list must be ordered

function merge(l1, l2) {
    // sorts ascending.
    let result = [];
    let i1 = 0;
    let i2 = 0;
    while (i1 < l1.length && i2 < l2.length) {

        if (l1[i1] < l2[i2]) {
            result.push(l1[i1]);
            i1++;
        } else {
            result.push(l2[i2]);
            i2++;
        }
    }
    // concat create a new array just to add a few values.
    return result.concat(l1.slice(i1), l2.slice(i2));
}

function mergeSort(L) {
    // slicing will also create a new array. 
    if (L.length < 2) {
        return L;
    }

    // let iterations = Math.floor(Math.log2(L.length));
    let center = L.length % 2 === 0 ? L.length / 2 : (L.length - 1) / 2;
    let l1 = L.slice(0, center);
    let l2 = L.slice(center);

    return merge(
        mergeSort(l1),
        mergeSort(l2)
    )
}
let L = [1, 2, 4, 3]
console.log(mergeSort([1, 2, 4, 3]));
console.log(mergeSort([34,23,86,3,5,6,885,3,2,53,2,4,7,45,2,45,24,6,6,5.6]));