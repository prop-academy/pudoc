import React, { Component } from 'react'
import './style.scss'

const floorStyle = {
    margin: '100px auto',
    display: 'grid',
    gridTemplate: 'repeat(5, 1fr) / repeat(5, 1fr)',
    placeItems: 'stretch stretch',
    height: '500px',
    width: '500px',
}

function random(min, max) {
    var num = Math.floor(Math.random() * (max - min + 1)) + min;
    return num;
}

function randomColor() {
    return 'rgb(' + random(0, 255) + ',' + random(0, 255) + ',' + random(0, 255) + ')'
}

const cellStyle = {
    border: '1px solid black',
}

class App extends Component {

    constructor(props) {
        super(props);
        // is executed at loading
        this.state = {
            colors: this.createColors()
        }
        setInterval(() => {
            this.setState({
                colors: this.createColors()
            })
        }, 1000)
    }
    
    createColors = () => {
        const colors = [];
        for (let i = 0; i < 25; i++) {
            colors.push(
                randomColor()
            );
        }
        return colors
    }

    render() {
        return (
            <div style={floorStyle}>
                {this.state.colors.map((color, i) => {
                    // console.log(color)
                //    return <div style={{...cellColor, ...cellStyle}} key={i}></div>
                   return <div style={{backgroundColor: color}} key={i} id={i}></div>
                })}
            </div>
        )
    }
}

export default App;