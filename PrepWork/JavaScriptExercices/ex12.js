// Write a function that receives an array of two numbers as argument
//  and returns the sum of those two numbers and all numbers between them.

function add_all(arr) {
    if (!arr.constructor === Array && arr.lenght !== 2) return 'Please input an array with two numbers.'
    start = arr[0];
    end = arr[1];
    diff = end - start;
    let pairs = diff / 2;

    if (diff % 2 !== 0) {
        // 3,6, diff = 3
        return (start + end) * Math.ceil(pairs)
    } else {
        // 11 - 5 = 6; 3 * (11 + 5) + ( 11+5 ) / 2
        return (start + end) * pairs + (end + start) / 2
    }

}


console.log(add_all([1, 4])) // 10
console.log(add_all([5, 10])) // 45
console.log(add_all([9, 10])) // 19
console.log(add_all([0, 0])) // 0
console.log(add_all([-1, 1])) // 0


