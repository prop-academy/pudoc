# Docker pipelines with Gitlab

## Short variable overview

- `$CI_PIPELINE_ID`: used as a unique identifier to build and then retag the image the way you want.
- `$CI_COMMIT_REF_NAME`: is the branch name
- `$CI_REGISTRY_IMAGE`: is the registry name without the tag, e.g. registry.gitlab.com/my_group/my_project:latest

## Best practices

### Clean up stage

- https://gist.github.com/bastman/5b57ddb3c11942094f8d0a97d461b430

## Questions

- How is the isolation of stages in CI CD?
- How do artifacts get passed?
- Under which user is the gitlab runner running the jobs?
- Is the runner running with root permissions?
- Cleanup: There are still containers lying around and using space for the suggested clean up stage. Is it safe to run docker system prune when
 the application is deployed and running?
- What helps the Diffie-Hellmann thing actually to ssl?
- Where is gitlab building the app? Is the repo pulled from gitlab or where does it know where to get the new code?
- Where is the docker-compose file stored on the remote droplet?
- Can a runner register a group AND a separate repo?
- Are there implications when running containers not as root?

Colin

- build failing : delete builds directory in `/home/gitlab-runner/builds`